// OpentTxl-C Version 11 debugger
// Copyright 2023, James R Cordy and others

// Used only in xform.c transformer

// Initialization
extern void debugger_initializeDebugger (void);

// Kinds of debugger breakpoints
enum debugger_breakpointT {
    debug_startup, debug_shutdown, debug_ruleEntry, debug_ruleExit, debug_matchEntry, debug_matchExit,
    debug_deconstructExit, debug_constructEntry, debug_constructExit, debug_conditionExit, debug_importExit,
    debug_exportEntry, debug_exportExit, debug_history
};
#define nDebuggerBreakpoints 14

// Breakpoints
extern bool debugger_isbreakpoint (const tokenT ruleName);
extern void debugger_breakpoint (const enum debugger_breakpointT kind, const tokenT ruleName, const int partRef, const treePT scope, 
        const struct transformer_ruleEnvironmentT *ruleEnvironment, const bool success);

