// OpentTxl-C Version 11 limits
// Copyright 2023, James R. Cordy and others

// TXL processor limits
// Maximum limits on sizes of parsing / transformation data structures and processing steps

#ifdef TXLMAIN
// Initializer
extern void limits (void);
#endif

// All arrays are 1-origin, with index 0 representing undefined
#define NOT_FOUND   0
#define UNUSED      -1   // hopefully cause a crash if used

// Maximum TXL program source files
#define maxFiles 1024 

// Maximum TXL command line arguments
#define maxProgramArguments 32

// Maximum number of preprocessor symbols 
#define maxIfdefSymbols 64

// Maximum preprocessor depth
#define maxIfdefDepth 32

// Maximimum line length - limits input and output line length
#define maxLineLength maxLongStringLength       // 1 Mb - 1

// Maximum output indentation, in characters
#define indentLimit     256

// Maximum include file depth in a TXL program
#define maxIncludeDepth 8

// Maximum include file libraries 
#define maxTxlIncludeLibs 10

// Maximum defined symbols in the TXL bootstrap grammar
#define maxBootstrapSymbols 100

// Maximum number of keywords in each of bootstrap and TXL program
#define maxKeys 2048

// Maximum total compound tokens in a TXL program
#define maxCompoundTokens 128

// Maximum different token patterns in a TXL program
#define maxTokenPatterns 128
#define maxTokenPatternLinks (25 * maxTokenPatterns)

// Maximum total comment brackets in a TXL program
#define maxCommentTokens 32     // (pairs)

// Number of parameters to a rule
#define maxParameters 16
#define avgParameters 4

// Number of local vars in a rule
#define maxLocalVars 65535      // unsigned short
#define avgLocalVars 128

// Maximum unique sub-rule calls in a rule
#define maxRuleCalls 65535      // unsigned short

// Number of conditions, constructors and deconstructors preceding
// or following a pattern in a rule
#define maxParts 65535          // unsigned short
#define avgParts 64             // allow for very complex rule sets

// Maximum length of any rule pattern or replacement, in tokens
#define maxPatternTokens 4096

// Maximum parsing depth without an accept before infinite recursion checking
// takes effect
#define maxBlindParseDepth 10

// Maximum recursion levels before we assume it is infinite
#define maxLeftRecursion 10

// Maximum number of alternatives in a choice or elements in a sequence
#define maxDefineKids 65535     // unsigned short

// Maximum TXL internal pre-reserved trees and kids
#define reservedTrees 256
#define reservedKids  256

// Maximum total defined symbols in a TXL program
extern int maxSymbols; // = 16384 + (options.txlSize div 200) * 2048 = 16384, 18432 (200), 20480 (400) .. 26624 (1000)

// Maximum number of rules in a TXL program
extern int maxRules; // = 4096 + (options.txlSize div 200) * 1024 = 4096, 5120 (200), 6144 (400) .. 9216 (1000)

// Maximum different token texts, including identifiers, strings and numbers,
// in entire TXL program - must be a power of 2!
extern int maxIdents;  // = vmaxIdents

// Maximum total characters in token texts
extern int maxIdentChars; // maxIdents * 32     // (statistical estimate of ratio)

// Maximum total length of any input,
// including each of: TXL bootstrap grammar, TXL program, input source
extern int maxTokens; // options.transformSize * 4000 

// Maximum number of lines in any single input,
// including each of: TXL bootstrap grammar, TXL program, input source
extern int maxLines; // = maxTokens div 10

// Maximum parsing depth in any parse
// Normally should allow for the length of the whole input file
extern int maxParseDepth; // = maxLines

// Maximum parsing cycles in any parse
// Normally should allow for the length of the whole input file
extern int maxParseCycles; // = min (1000000 + 100 * maxTokens, 500000000)

// Maximum rule call depth
// Normally should allow for at least the length of the whole input file
// so that a recursive function can examine every line
extern int maxCallDepth; // = maxParseDepth div 4

// Limits on total number of trees and kids in all trees
extern int maxTrees; // = options.transformSize * 50000
extern int maxKids;  // = (maxTrees * 3) div 2

// Limit on TXL recursion stack use 
extern addressint maxStackUse; // = defaultStackUse

