// OpentTxl-C Version 11 scanner
// Copyright 2023, James R Cordy and others

// Initialization 
#ifdef TXLMAIN
extern void scanner (void);
#endif

// Used in txl.c main process and xform_predef.h built-in functions 
extern void scanner_tokenize (const string fileNameOrText, const bool isFile, const bool isTxlSource);

// Used in comprul.c rule compiler, xform.c transformer and unparse.h unparser
extern bool scanner_keyP (const tokenT token);

// Used in loadstor.c compiled application load/store and txl.c main process statistics
extern int scanner_nKeys;
extern int scanner_nCompounds;
extern int scanner_nComments;
extern int scanner_nPatterns;

// Used only in loadstor.c compiled application load/store
#ifdef LOADSTORE
struct scanner_compoundT {
    int length;
    string literal;
};

typedef short scanner_patternCodeT;

typedef scanner_patternCodeT scanner_patternT[maxStringLength];

struct scanner_patternEntryT {
    enum treeKindT kind;
    tokenT name;
    scanner_patternT pattern;
    int length;
};

extern struct scanner_compoundT scanner_compoundTokens[maxCompoundTokens + 1];
extern int scanner_compoundIndex[ASCII];

extern tokenT scanner_commentStart[maxCommentTokens];
extern tokenT scanner_commentEnd[maxCommentTokens];

extern struct scanner_patternEntryT scanner_tokenPatterns[maxTokenPatterns];
extern int scanner_patternIndex[ASCII];
extern int scanner_patternLink[maxTokenPatternLinks];
extern int scanner_nPatternLinks;
extern int scanner_patternNLCommentIndex;

extern tokenT scanner_keywordTokens[maxKeys];
extern int scanner_nTxlKeys;
extern int scanner_lastKey;
#endif
