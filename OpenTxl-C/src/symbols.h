// OpentTxl-C Version 11 symbol table
// Copyright 2023, James R. Cordy and others

// The TXL program nonterminal symbol table 
// Nonterminal defines are compiled into grammar trees for the defined nonterminal symbols, stored here

#ifdef TXLMAIN
// Initialization
extern void symbol (void);
#endif

// 1-origin [1 .. maxSymbols]
extern array (treePT, symbol_symbols);
extern int symbol_nSymbols;

extern int symbol_enterSymbol (tokenT partId, enum treeKindT kind);
extern int symbol_lookupSymbol (tokenT partId);
extern int symbol_findSymbol (tokenT partId);
