The OpenTxl-C source code and build process 

To build the production version of OpenTxl-C, use the command 
"make" ("nmake -f Makefile.WIN" on Windows) in this directory.

For a basic functionality test, use the command "make" 
("nmake -f Makefile-WIN" on Windows) in the test/ subdirectory.

For a full TXL language regression test comparing this version of TXL
with your previous installed one, use the command "make" 
("nmake -f Makefile-WIN" on Windows) in the test/regression/ subdirectory. 

See the README.txt files in those directories for further information.
