## 函数内部变量尽量使用参数传递

能传递参数的尽量传递参数，不要使用闭包。闭包作为参数会多一次闭包创建和访问。

【反例】

``` TypeScript
let arr = [0, 1, 2];

function foo() {
  // arr 尽量通过参数传递
  return arr[0] + arr[1];
}
foo();
```

【正例】

``` TypeScript
let arr = [0, 1, 2];

function foo(array: Array) : number {
  // arr 尽量通过参数传递
  return array[0] + array[1];
}
foo(arr);
```