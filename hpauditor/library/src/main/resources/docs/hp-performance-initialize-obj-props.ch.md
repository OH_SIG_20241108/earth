## 对象构造初始化

对象构造的时候，要给默认值初始值，不访问未初始化的属性。
```TypeScript
// 不要访问未初始化的属性
class A {
  x：number；
}
// 构造函数中要对象的属性进行初始化
class A {
  x：number；
  constuctor() {
  }
}
let a = new A();
// 这个时候x未赋值就直接使用，这个时候会访问整个原型链
print(a.x);

```
针对class的实例的构造，推荐使用如下初始化方式。
```TypeScript
// 推荐一：声明初始化
class A {
  x: number = 0;
}
// 推荐二：构造函数直接赋初值
class A {
  constuctor() {
    this.x = 0;
  }
}
let a = new A();
print(a.x);
```
