## 避免设置冗余系统回调监听

在优化回调接口时，若回调函数体内不包含任何业务逻辑代码，可以将其删除。日志输出的影响对系统性能不容忽视。冗余的Trace和日志打印不仅增加性能开销，还降低代码的可读性和维护性，同时推高存储成本。因此，务必采取积极措施，定期审查和清理那些不必要的日志和Trace，以确保系统能够高效、稳定地运行。

**反例：**

下面代码示例演示了，在Button按钮的点击事件回调函数.onClick()中，添加冗余的Trace和日志打印操作；而在.onAreaChange回调中，无任何实际执行的代码逻辑。

```ts
// onClick回调场景反例
@Component
struct NegativeOfOnClick {
  build() {
    Button('Click', { type: ButtonType.Normal, stateEffect: true })
      .onClick(() => {
        hitrace.startTrace("ButtonClick", 1004);
        hilog.info(1001, 'Click', 'ButtonType.Normal')
        hitrace.finishTrace("ButtonClick", 1004);
        // 业务逻辑
        // ...
      })
      .onAreaChange((oldValue: Area, newValue: Area) => {
        // 无任何代码
      })
   }
}
```

**正例：**

在处理Button按钮的点击事件回调函数onClick()时，应当避免添加无意义的Trace追踪及日志打印，以保持代码的精简和执行效率。仅在开发调试阶段或者定位问题时，才适宜插入这类跟踪和日志输出语句，在应用的正式运行环境下则应去除这些冗余部分，从而防止对程序性能造成潜在影响并提升日志信息的有效性。对于处理.onAreaChange回调函数时无任何业务逻辑代码的情形，可以直接安全地删除该空回调，避免系统对无意义事件做出响应，从而节省资源并提高程序运行效率。

```ts
// onClick回调场景正例
@Component
struct PositiveOfOnClick {
  build() {
    Button('Click', { type: ButtonType.Normal, stateEffect: true })
      .onClick(() => {
        // 业务逻辑
        // ...
    })
}
```