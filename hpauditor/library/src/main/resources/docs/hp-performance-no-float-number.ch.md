## 正确使用浮点数

不要使用浮点数作为文本大小和坐标的值


**【反例】**

```javascript

{
	"command": "DrawTextBlob",
	"visible":true,
	"x" : 74.5078,     // float number
	"y" : 48.1724,    // float number
	"bounds": [
		-23.1483, 
		-44.6069, 
		139.339, 
		11.4474
	], 
	"paint":{
		"color": [
			255, 
			255, 
			255, 
			255
		]
	},
	"runs":[ 
	  {
		"font":{
			"subpixelText":true,
			"textSize" : 42.2414,   //float number
			"edging": "antialias",
			"hinting": "slight", 
			"typeface": { 
				"data": "/data/0" 
			}
		}
	  }
	]
}

```

**【正例】**

```javascript
{
 "command": "DrawTextBlob",
 "visible": true,
  "x": 75,
   "y": 48,
    "bounds": [
            -23,
            -45,
            139,
             11
     ],
     "paint": { 
            "color": [
                    255,
                    255,
                    255, 
                    255
            ]
     }, 
     "runs": [
    { 
            "font": { 
                    "subpixelText": true,
                    "textSize": 42,
                    "edging": "antialias",
                    "hinting": "slight", 
                    "typeface": { 
                      "data": "/data/0" 
                    } 
            } 
    }
   ] 
};
```