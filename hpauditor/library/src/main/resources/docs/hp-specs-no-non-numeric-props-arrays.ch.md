## 不要使用可变参数的Array构造函数

**说明：** 通常不鼓励使用构造函数`new Array`的方法来构造新数组，因为当构造函数只有一个参数的时候，可能会导致意外情况，另外Array的全局定义也可能被重新修改，所以提倡使用数组文字表示法来创建数组，也就是`[]`表示法。
**反例：**

```javascript
const arr1 = new Array(x1, x2, x3);
const arr2 = new Array(x1, x2);
const arr3 = new Array(x1);
const arr4 = new Array();
```

除了第三种情况之外，其他都可以正常工作，如果`x1`是个整数，那么`arr3`就是长度为`x1`，值都为`undefined`的数组。如果`x1`是其他任何数字，那么将抛出异常，如果它是其他任何东西，那么它将是单元数组。

**正例：**

```javascript
const arr1 = [x1, x2, x3];
const arr2 = [x1, x2];
const arr3 = [x1];
const arr4 = [];
```

这种写法，就会省去很多麻烦。

同理，对于对象，同样不要使用`new Object()`，而是使用`{}`来创建