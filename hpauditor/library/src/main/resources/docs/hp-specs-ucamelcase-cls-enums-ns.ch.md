## 类名、枚举名、命名空间名采用`UpperCamelCase`风格。

**正例：**

```javascript
// 类名
class User {
  constructor(username) {
    this.username = username;
  }
  
  sayHi() {
    console.log(`hi, ${this.username}`);
  }
}

// 枚举名
const UserType = {
  TEACHER: 0,
  STUDENT: 1
};

// 命名空间
const Base64Utils = {
  encrypt: function(text) {
    // todo encrypt
  },
  decrypt: function(text) {
    // todo decrypt
  }
};
```