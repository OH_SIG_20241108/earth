% Ark TS Code Auditor
% James Cordy et al., Huawei Technologies

% August 2023 (Rev. Jan 2024)

% Typescript grammar
#pragma -comment
include "bom.grm"
include "js-ts.grm"

% Ark ETS grammar overrides
include "ets.grm"

% TXL string utility library
include "stringutils.rul"

% Standard ArkTS formatting - issue #72
include "tsformat.grm"

% Auditing overrides
include "tsaudit.grm"

% Line number and source file inference
include "tslinefile.rul"

% Lightweight type inference
include "tsinfer.rul"

% Default rule set and severity
include "tsrulecontrol.rul"


% Main auditing rule

function main
    match [program]
        _ [program]
end function
