Applications require rapid feedback on user input to improve the interactive experience, so this article provides the following methods to improve application response speed.

- Avoid the main thread being blocked by non-UI tasks
- Reduce the number of component refreshes

# Reduce the number of components refreshed

When the application refreshes the page, it is necessary to reduce the number of refreshed components as much as possible. If the number is too large, it will take too long to perform measurement and layout on the main thread. AboutToAppear(), aboutToDisappear() method increases the main thread load.

## Limit refresh scope using containers

Counter example: If there is a component in the container that is included in the if condition, the change in the if condition result will trigger the creation and destruction of the component. If the layout of the container is affected at this time, all components in the container will be refreshed, causing the main thread UI refresh to take too long. .

The Text('New Page') component in the following code is controlled by the state variable isVisible. It is created when isVisible is true and destroyed when it is false. When isVisible changes, all components within the Stack container are refreshed:

```
@Entry
@Component
struct StackExample5 {
  @State isVisible : boolean = false;

  build() {
    Column() {
      Stack({alignContent: Alignment.Top}) {
        Text().width('100%').height('70%').backgroundColor(0xd2cab3)
          .align(Alignment.Center).textAlign(TextAlign.Center);

        // 此处省略100个相同的背景Text组件

        if (this.isVisible) {
          Text('New Page').height("100%").height("70%").backgroundColor(0xd2cab3)
            .align(Alignment.Center).textAlign(TextAlign.Center);
        }
      }
      Button("press").onClick(() => {
        this.isVisible = !(this.isVisible);
      })
    }
  }
}
```

Suggestion: For this kind of component controlled by state variables, wrap a container around the if to reduce the refresh scope.

```
@Entry
@Component
struct StackExample6 {
  @State isVisible : boolean = false;

  build() {
    Column() {
      Stack({alignContent: Alignment.Top}) {
        Text().width('100%').height('70%').backgroundColor(0xd2cab3)
          .align(Alignment.Center).textAlign(TextAlign.Center);

        // 此处省略100个相同的背景Text组件

        Stack() {
          if (this.isVisible) {
            Text('New Page').height("100%").height("70%").backgroundColor(0xd2cab3)
              .align(Alignment.Center).textAlign(TextAlign.Center);
          }
        }.width('100%').height('70%')
      }
      Button("press").onClick(() => {
        this.isVisible = !(this.isVisible);
      })
    }
  }
}
```

