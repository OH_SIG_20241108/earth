## 不要使用arguments，可以选择rest语法替代

rest参数是一个真正的数组，也就是说能够在它上面直接使用所有的数组方法，比如sort，map，forEach或pop，而arguments是一个类数组。因此，应选择使用rest语法替代arguments。另外，rest参数必须是列表中的最后一个参数。

**【反例】**

```javascript
function concatenateAll() {
  // 因为arguments是类数组，不能直接使用join方法，需要先转换为真正的数组
  const args = Array.prototype.slice.call(arguments);   
  return args.join('');
}
```

**【正例】**

```javascript
function concatenateAll(...args) {
  return args.join('');
}
```
