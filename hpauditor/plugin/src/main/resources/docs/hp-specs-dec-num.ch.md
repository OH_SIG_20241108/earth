## 不要省略浮点数小数点前后的0

在JavaScript中，浮点值会包含一个小数点，没有要求小数点之前或之后必须有一个数字。虽然不是一个语法错误，但这种格式的数字使真正的小数和点操作符变的难以区分。由于这个原因，必须在小数点前面和后面有一个数字，以明确表明是要创建一个小数。

**【反例】**

```javascript
const num = .5;
const num = 2.;
const num = -.7;
```

**【正例】**

```javascript
const num = 0.5;
const num = 2.0;
const num = -0.7;