## 浮点型数据判断相等不要直接使用==或===

**【级别】规则**

**【描述】**

由于浮点数在计算机表示中存在精度的问题，数学上相等的数字，经过运算后，其浮点数表示可能不再相等，因而不能使用相等运算符==或===判断浮点数是否相等。

**【反例】**

```javascript
0.1 + 0.2 == 0.3; // false
0.1 + 0.2 === 0.3; // false
```

**【正例】**

```javascript
const EPSILON = 1e-6;
const num1 = 0.1;
const num2 = 0.2;
const sum = 0.3;
if(Math.abs(num1 + num2 - sum) < EPSILON) {
  ...
}
```