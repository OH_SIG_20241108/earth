##  Initialize object properties
Provide default initial values when constructing an object and do not access uninitialized properties.

Bad ts code
```TypeScript
// x is neither initialized inside nor outside the constructor.
class A {
  x: number;
  constructor() {}
}
// -- OR --
class A {
  x: number;
}

let a = new A();
// x is not assigned a value.
console.log(a.x); // undefined
```