## 强制使用类型导入的一致性

**【级别】规则**

**【描述】**

如果导入类型（type），将导入类型和导入其他对象分开写。

**【反例】**

```javascript
import { Foo } from 'Foo';
import Bar from 'Bar';
type T = Foo;
const x: Bar = 1;
```

**【正例】**

```javascript
import type { Foo } from 'Foo';
import type Bar from 'Bar';
type T = Foo;
const x: Bar = 1;
```