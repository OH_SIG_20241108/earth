## 禁止使用eval()


使用eval()会让程序比较混乱，导致可读性较差。

**【反例】**

```javascript
console.log(eval({ a:2 })); // 输出[object Object] 
console.log(eval('"a" + 2')); // 输出'a2' 
console.log(eval('{ a: 2 }')); // 输出2 
console.log(eval('let value = 1 + 1;')); // 输出undefined
```
