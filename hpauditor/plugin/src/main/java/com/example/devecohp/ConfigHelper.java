package com.example.devecohp;

import com.intellij.psi.PsiFile;

import java.util.regex.Pattern;

public class ConfigHelper {
    private static Pattern[] configFilePattern = new Pattern[]{
            Pattern.compile(".*wrapper.js")
    };
    public static boolean isConfigFile(PsiFile file){
        for(Pattern p: configFilePattern){
            if(p.matcher(file.getVirtualFile().getPath()).find()){
                return true;
            }
        }
        return false;
    }
}
