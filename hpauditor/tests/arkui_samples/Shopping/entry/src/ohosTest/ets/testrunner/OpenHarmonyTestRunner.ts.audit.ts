/* HPAudit: Always use strict mode (fixed) : hp-specs-use-strict-mode : 1 : 0 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
"use strict";
/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import TestRunner from '@ohos.application.testRunner';
import AbilityDelegatorRegistry from '@ohos.app.ability.abilityDelegatorRegistry';
/* HPAudit: Use const or let when declaring variables (fixed) : hp-specs-use-const-or-let-for-vars : 1 : 19 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
let abilityDelegator = undefined
/* HPAudit: Use const or let when declaring variables (fixed) : hp-specs-use-const-or-let-for-vars : 1 : 20 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
let abilityDelegatorArguments = undefined
/* HPAudit: Explicitly declare return types of functions and methods (fixed) : hp-specs-explicit-return-types : 1 : 22 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
async function onAbilityCreateCallback(): void {
  console.log('onAbilityCreateCallback');
}
/* HPAudit: Explicitly declare return types of functions and methods (fixed) : hp-specs-explicit-return-types : 1 : 26 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
/* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 26 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
async function addAbilityMonitorCallback(err: any): void {
  console.log('addAbilityMonitorCallback');
}
export default class OpenHarmonyTestRunner implements TestRunner {
  constructor() {
  }
  /* HPAudit: Explicitly declare return types of functions and methods (fixed) : hp-specs-explicit-return-types : 1 : 34 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
  onPrepare(): void {
    console.log('onPrepare');
  }
  /* HPAudit: Explicitly declare return types of functions and methods (fixed) : hp-specs-explicit-return-types : 1 : 38 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
  async onRun(): void {
    console.log('onRun');
    abilityDelegatorArguments = AbilityDelegatorRegistry.getArguments()
    abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator()
    /* HPAudit: Use const or let when declaring variables (fixed) : hp-specs-use-const-or-let-for-vars : 1 : 42 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
    const bundleName = abilityDelegatorArguments.bundleName
    /* HPAudit: Use const or let when declaring variables (fixed) : hp-specs-use-const-or-let-for-vars : 1 : 43 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
    const parameters = abilityDelegatorArguments.parameters
    /* HPAudit: Declare unchanged variables as const (fixed) : hp-performance-unchanged-const-vars : 1 : 44 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
    /* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 44 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
    const moduleName = parameters["moduleName"]
    /* HPAudit: Declare unchanged variables as const (fixed) : hp-performance-unchanged-const-vars : 1 : 45 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
    const lMonitor = { 
        abilityName: "TestAbility",
        onAbilityCreate: onAbilityCreateCallback,
      };
    abilityDelegator.addAbilityMonitor(lMonitor, addAbilityMonitorCallback)
    /* HPAudit: Declare unchanged variables as const (fixed) : hp-performance-unchanged-const-vars : 1 : 50 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
    const want = { 
        abilityName: "TestAbility",
        bundleName: bundleName,
        moduleName: moduleName
      };
    /* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 55 : samples/Shopping/entry/src/ohosTest/ets/testrunner/OpenHarmonyTestRunner.ts */
    abilityDelegator.startAbility(want, (err: any, data: any) => {
      console.log('startAbility' + err + data);
    });
  }
}
