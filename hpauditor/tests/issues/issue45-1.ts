// It is difficult to understand the value assignment in the control judgment.
let isFoo = false;
if (isFoo = true) {
  console.log('Hello');
}

// Assignment in while loop condition.
let arr = [1, 2, 3];
let sum = 0, num = 0;
while (num = arr.pop()) {
  sum += num;
}
console.log(sum);

// Assignment in do loop condition.
arr = [1, 2, 3];
sum = 0;
num = arr.pop();
do {
  sum += num;
} while (num = arr.pop());
console.log(sum);
