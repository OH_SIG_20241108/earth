/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue66.ts */
"use strict";
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue66.ts */
/* HPAudit: Do not use negative boolean variable and function names : hp-specs-no-negative-bool-vars-funcs : 1 : 1 : issue66.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 1 : issue66.ts */
const IS_NOT_ERROR: boolean = true;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue66.ts */
/* HPAudit: Do not use negative boolean variable and function names : hp-specs-no-negative-bool-vars-funcs : 1 : 2 : issue66.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 2 : issue66.ts */
const IS_NOT_FOUND: boolean = false;
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 4 : issue66.ts */
/* HPAudit: Do not use negative boolean variable and function names : hp-specs-no-negative-bool-vars-funcs : 1 : 4 : issue66.ts */
function noError(): boolean {
  return true;
}
function error(): boolean {
  return ! IS_NOT_ERROR;
}
function next(): boolean {
  return ! IS_NOT_FOUND;
}
