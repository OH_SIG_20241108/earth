import { BreakpointConstants, BreakpointSystem } from '@ohos/common';
import { HomePage } from '@ohos/content';
import { PageConstants } from '../constants/PageConstants';
import { tabBarArray, TabBarInfoModel } from '../viewmodel/TabBarViewModel';

/**
 * 主页面
 */
@Entry
@Component
struct MainPage {
  @StorageProp('currentBreakpoint') currentBreakpoint: string = BreakpointConstants.BREAKPOINT_SM;
  @StorageLink('IndexPage') currentPageIndex: number = PageConstants.HOME_TAB_INDEX;
  private controller: TabsController = new TabsController();
  build() {
    Navigation() {
      Column() {
        Tabs({ 
          barPosition: this.currentBreakpoint === BreakpointConstants.BREAKPOINT_LG ? BarPosition.Start : BarPosition.End,
          index: this.currentPageIndex,
          controller: this.controller
        }) {
          TabContent() {
            HomePage()
          }
            .linearGradient({ 
              colors: [[$r('app.color.page_background_color_top'), 0.0], [$r('app.color.page_background_color_bottom'), 0.5]]
            })
            .tabBar(this.TabBarNavigation(tabBarArray[PageConstants.HOME_TAB_INDEX]))
        }
          .backgroundColor(Color.White)
          .barMode(BarMode.Fixed)
      }
    }
      .hideNavBar(true)
      .hideTitleBar(true)
  }
  @Builder TabBarNavigation(tabBar: TabBarInfoModel) {
    Column({ 
      space: PageConstants.BUTTON_SPACE
    }) {
      Image(this.currentPageIndex === tabBar.index ? tabBar.selectImg : tabBar.unselectImg)
        .objectFit(ImageFit.Contain)
      Text(tabBar.title)
        .fontColor(this.currentPageIndex === tabBar.index ? $r('app.color.focus_color') : Color.Black)
    }
      .backgroundColor(Color.White)
  }
}
