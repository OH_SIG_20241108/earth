// The return value type is not declared void.
function test() {
  console.log('Hi');
}
  
// The return value type number is not declared.
function fn() {
  return 1;
};
  
// The return value type is not declared as string.
let arrowFn = () => 'test';

class Test {
  // If there is no return value, the return value type is not declared void.
  method() {
    console.log('Test');
  }
}
