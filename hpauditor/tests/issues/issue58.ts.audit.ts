/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue58.ts */
"use strict";
// Enforce consistency of using type imports across files

// ==========================
// Foo.ts
interface Foo {
  propA: string;
  propB: string;
}
export type { Foo };

// Bar.ts
export default interface Bar {
  propA: number;
}
// ==========================


// ==========================

// Main file
import { Foo } from './Foo'; // Wrong, Foo is exported as a type, so should be imported "type"
import Bar from './Bar'; // Wrong, Bar is an exported interface, so should be imported "type"
type T = Foo;
const x: Bar = { 
    propA: 1
  };
