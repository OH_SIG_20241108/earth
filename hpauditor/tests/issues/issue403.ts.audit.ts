/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue403.ts */
"use strict";
function foo(): number {
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue403.ts */
  /* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 2 : issue403.ts */
  const LEFT = 5;
  // NOT a control assignment
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 4 : issue403.ts */
  /* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 4 : issue403.ts */
  const right = LEFT === 4 ? LEFT - 5 : 0;
  return right;
}
function foo2(): number {
  let left = 5;
  // NOT a control assignment
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 11 : issue403.ts */
  const right = left = 4 ? left - 5 : 0;
  return right;
}
function foo3(): number {
  let left = 5;
  // YES a control assignment
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 18 : issue403.ts */
  /* HPAudit: Do not assign variables in control conditions : hp-specs-no-vars-control-condition-expns : 1 : 18 : issue403.ts */
  left = 4
  const right = (left) ? left - 5 : 0;
  return right;
}
function foo4(): number {
  let left = 5;
  // YES a control assignment
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 25 : issue403.ts */
  /* HPAudit: Do not assign variables in control conditions : hp-specs-no-vars-control-condition-expns : 1 : 25 : issue403.ts */
  left = 4
  /* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 25 : issue403.ts */
  const right = left === (left) ? left - 5 : 0;
  return right;
}
