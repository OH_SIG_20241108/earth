// Note: with() is unsupported in strict mode, compiler will emit error
const foo: { x: number } = { x: 5 };
with (foo) {
  let x: number = 3;
  console.log(x); // output 3
}

console.log(foo.x); // output 5
