/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue32.ts */
"use strict";
/* HPAudit: Do not use eval() : hp-specs-no-eval : 1 : 1 : issue32.ts */
console.log(eval({ 
  a: 2
})); // outputs { a: 2 }
/* HPAudit: Do not use eval() : hp-specs-no-eval : 1 : 2 : issue32.ts */
console.log(eval('"a" + 2')); // outputs 'a2' 
/* HPAudit: Do not use eval() : hp-specs-no-eval : 1 : 3 : issue32.ts */
console.log(eval('{ a: 2 }')); // outputs 2 
/* HPAudit: Do not use eval() : hp-specs-no-eval : 1 : 4 : issue32.ts */
console.log(eval('let value = 1 + 1;'));
