/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue57.ts */
"use strict";
// Enforce consistency of type exports
interface ButtonProps {
  onClick: () => void;
}
type ButtonType = number;
class Button implements ButtonProps {
  /* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 9 : issue57.ts */
  onClick(): void {
    console.log('button!');
  }
}

// 1. Interfaces/types should not be mixed with objects/classes in export statements

// 2. Interfaces/types should be exported as 'type'
export { Button, ButtonProps, ButtonType }; // wrong, Button is a class, ButtonProps is an interface, and ButtonType is a type

                                            // wrong, both ButtonProps and ButtomType should be exported 'type'

// -- or --
export { Button, type ButtonProps };
