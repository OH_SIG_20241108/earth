class O1 {
  x: string | undefined = "";
  y: string | undefined = "";
}
let obj: O1 = {x: "", y: ""};

obj.x = "xxx";
obj.y = "yyy";
delete obj.x;

class O2 {
  a: string | undefined = "";
  b: string | undefined = "";
}
let obj2: O2 = {a: "", b: ""};

obj2.a = "xxx";
obj2.b = "yyy";
