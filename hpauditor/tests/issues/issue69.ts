// Without braces, only the first statement after the conditional statement and loop belongs to it.
let isGood: boolean = true;
if (isGood)
  console.log('success');

for (let idx = 0; idx < 5; idx++)
  console.log(idx);

let i = 0;
while (i < 5)
  i++;
