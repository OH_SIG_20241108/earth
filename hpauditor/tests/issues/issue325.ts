function  foo(cellAndSpan: CellAndSpan): number[] {
  let result: [number, number];
  let resultSpan: [number, number];
  this.lazyInitTempRectStack();

  let pixelX: number = cellAndSpan.getCellX();
  let pixelY: number = cellAndSpan.getCellY();

  let bestDistance: number = Number.MAX_VALUE;
  let bestRect: RectItem = new RectItem(-1, -1, -1, -1);
  const validRegions: Stack<RectItem> = new Stack<RectItem>();
  for (let i = 0; i < this.mRow - cellAndSpan.getSpanY() + 1; i++) {
    for (let j = 0; j < this.mColumn - cellAndSpan.getSpanX() + 1; j++) {
      let cellCoords: number[];
      cellCoords = this.cellToCenterPoint(j, i, cellAndSpan.getSpanX(), cellAndSpan.getSpanY());

      let currentRect: RectItem = this.mTempRectStack.pop();
      if (!currentRect) {
        continue;
      }
      currentRect.set(j, i, j - 1, i - 1);
      let isContained: boolean = this.isContained4FindNearestArea(validRegions, currentRect);
      validRegions.push(currentRect);
      let distance: number = Math.hypot(cellCoords[0] - pixelX, cellCoords[1] - pixelY);

      if ((distance <= bestDistance && !isContained) || currentRect.containsRect(bestRect)) {
        bestDistance = distance;
        result = [j, i];
        if (resultSpan != null) {
          resultSpan = [-1, -1];
        }
        bestRect.setRect(currentRect);
      }
    }
  }
  if (bestDistance === Number.MAX_VALUE) {
    result = [-1, -1];
  }
  this.recycleTempRects(validRegions);
  return result;
}
