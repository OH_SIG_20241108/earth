/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue438.ts */
"use strict";
import sceneSessionManager from '@ohos.sceneSessionManager';
import { SCBSceneMode } from './SCBSceneInfo';
import { SCBSpecificSession } from './SCBSpecificSession';
import { LogDomain, LogHelper } from '../../../TsIndex';
import { SCBSceneSession } from './SCBSceneSession';
import { SCBSystemSceneSession, SystemBarType } from './SCBSystemSceneSession';
export enum InputState { UNDEFINED, SHOW_IN_BELOW_SCENE_PANEL, SHOW_IN_BELOW_SPECIFIC_SCENE, SHOW_IN_ABOVE_SCENE_PANEL, SHOW_IN_ABOVE_SPECIFIC_SCENE, SHOW_IN_ABOVE_SPLIT_SCENE };
export const enum SpecificPanelZOrder { ABOVE_SCENE_PANEL, ABOVE_SYSTEMUI, MEDIA_CONTROL_TEMP, ABOVE_KEYGUARD, VOLUME, SYSTEM_TOAST, PIP_PANEL, }
const SESSION_TYPE_TO_PANEL_ZORDER_MAP: Map<sceneSessionManager.SessionType, SpecificPanelZOrder> = new Map<sceneSessionManager.SessionType, SpecificPanelZOrder>();
const TAG = 'SCBSceneSessionManager';
const log = LogHelper.getLogHelper(LogDomain.SCB, TAG);
export class SCBSceneSessionManager {
  private getInputState(callingSession: SCBSceneSession | SCBSpecificSession | SCBSystemSceneSession): InputState {
    if (! callingSession) {
      return InputState.UNDEFINED;
    }
    /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 39 : issue438.ts */
    const specificPanel = SESSION_TYPE_TO_PANEL_ZORDER_MAP.get(callingSession.session.type);
    /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 40 : issue438.ts */
    const isScreenLock: boolean = this.isScreenLocked();
    /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 41 : issue438.ts */
    const showBelowKeyguard: boolean = ! callingSession.isShowWhenLocked || ! isScreenLock;
    const type: sceneSessionManager.SessionType = callingSession.session.type;
    log.showInfo('[SCBInput] getInputState, isScreenLocked: ' + isScreenLock + ', showBelowKeyguard: ' + showBelowKeyguard + ', specificPanel: ' + specificPanel + ', type: ' + type);



    // inputMethod show in belowScenePanel when mainScene not floating or is subScene
    if (showBelowKeyguard && ((callingSession instanceof SCBSceneSession && callingSession.sceneInfo.sceneMode !== SCBSceneMode.FLOATING) || (callingSession instanceof SCBSpecificSession && type === sceneSessionManager.SessionType.TYPE_SUB_APP &&
          // subWindow is shown in floating scene
    this.getFloatingSessionList().findIndex( (item) => {
      return item.session.persistentId === callingSession.session.parentId;
    }) === - 1))) {
      return InputState.SHOW_IN_BELOW_SCENE_PANEL;
    }
    return InputState.UNDEFINED;
  }
  public getFloatingSessionList = (): Array<SCBSceneSession> => {
      return [];
    };
  isScreenLocked(): boolean {
    return false;
  }
}
