/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue35.ts */
"use strict";
/* HPAudit: Use const or let when declaring variables : hp-specs-use-const-or-let-for-vars : 1 : 1 : issue35.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 1 : issue35.ts */
const NUM: number = 1;
/* HPAudit: Use const or let when declaring variables : hp-specs-use-const-or-let-for-vars : 1 : 2 : issue35.ts */
let count: number = 1;
/* HPAudit: Use const or let when declaring variables : hp-specs-use-const-or-let-for-vars : 1 : 3 : issue35.ts */
let isOK: boolean = true;
if (isOK) {
  count += 1;
  isOK = false;
}
console.log(count);
