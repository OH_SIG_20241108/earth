/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue46.ts */
"use strict";
// If any code paths explicitly return a value, each code paths should also return a value.

// The following are examples of functions possibly returning undefined.
// 1. Functions returning a boolean
/* HPAudit: Do not return undefined : hp-specs-no-return-undefined : 1 : 5 : issue46.ts */
function largerThan(a: number, b: number): boolean | undefined {
  if (a > b) {
    return true;
  } else if (a === b) {
    /* HPAudit: Do not return undefined : hp-specs-no-return-undefined : 1 : 9 : issue46.ts */
    return; // value not specified for return, returns undefined
  } else {
    /* HPAudit: Do not return undefined : hp-specs-no-return-undefined : 1 : 11 : issue46.ts */
    return undefined; // returns undefined
  }
}
/* HPAudit: Do not return undefined : hp-specs-no-return-undefined : 1 : 15 : issue46.ts */
function isHappy(happy: boolean): boolean | undefined {
  if (happy) {
    return true;
  }
  /* HPAudit: Do not return undefined : hp-specs-no-return-undefined : 1 : 19 : issue46.ts */
  return void 0; // returns undefined
}

// 2. Functions returning multiple types
/* HPAudit: Do not return undefined : hp-specs-no-return-undefined : 1 : 23 : issue46.ts */
function getRandom(num: number): number | string | undefined {
  if (num > 100) {
    return 50;
  } else if (num > 50) {
    return 'Hi!';
  }
  // no return statement, returns undefined
}
console.log(largerThan(2, 1));
console.log(largerThan(1, 2));
console.log(largerThan(2, 2));
console.log(isHappy(false));
console.log(getRandom(101));
console.log(getRandom(51));
console.log(getRandom(1));
