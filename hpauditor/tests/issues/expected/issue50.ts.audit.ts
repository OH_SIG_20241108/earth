/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue50.ts */
"use strict";
// Modifying indexOf() method of Array object.
/* HPAudit: Do not modify prototypes : hp-specs-no-modify-prototypes : 1 : 2 : issue50.ts */
Array.prototype.indexOf = function() {
    return - 1;
  }

// Adding a method to Array prototype.
/* HPAudit: Do not modify prototypes : hp-specs-no-modify-prototypes : 1 : 7 : issue50.ts */
Array.prototype.changeFirst = function() {
    this[0] = - 1;
  }
const arr = [1, 1, 1, 1, 1, 2, 1, 1, 1];
console.log(arr.indexOf(2)); //Output -1
arr.changeFirst();
console.log(arr);
