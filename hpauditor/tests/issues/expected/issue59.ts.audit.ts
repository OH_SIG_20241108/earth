/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue59.ts */
"use strict";
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 1 : issue59.ts */
/* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 1 : issue59.ts */
const X: any = 1;
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 2 : issue59.ts */
/* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 2 : issue59.ts */
const Y: any = true;
console.log(X + Y);
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 6 : issue59.ts */
/* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 6 : issue59.ts */
const AGE: any = 'seventeen';
/* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 8 : issue59.ts */
function greet(): any {
  return 'Hi!';
}
/* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 12 : issue59.ts */
function doesNothing(param: any): void {
}
/* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 14 : issue59.ts */
function doesNothing2(param: Array<any>): void {
}
/* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 16 : issue59.ts */
function sayRandom(num: number): any {
  if (num > 10) {
    return 'Hello!';
  } else if (num > 5) {
    return num;
  } else {
    return true;
  }
}
