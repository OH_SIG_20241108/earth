/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue410-2.ts */
"use strict";
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 1 : issue410-2.ts */
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue410-2.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 1 : issue410-2.ts */
const MAX_COUNT: number = 10;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue410-2.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 1 : issue410-2.ts */
const IS_COMPLETED: boolean = false;
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 3 : issue410-2.ts */
let pointX: number;
let pointY: number;
/* HPAudit: Do not assign multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 4 : issue410-2.ts */
pointX = 0;
pointY = 0;
