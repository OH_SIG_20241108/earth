/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue257.ts */
"use strict";
aboutToAppear()
{
  this.getStreaming()
  globalWindowClass.setKeepScreenOn(true, (err) => {
    if (err.code) {
      console.error('Failed to set the screen to be always on. Cause: ' + JSON.stringify(err));
      return;
    }
    console.info('Succeeded in setting the screen to be always on.');
  });
  /* HPAudit: Do not assign this to a variable : hp-specs-no-this-to-vars : 1 : 10 : issue257.ts */
  let _this = this
  this.conn.register( (error) => {
    Logger.info(TAG, "net is available, err: " + JSON.stringify(error));
  })
  this.conn.on('netUnavailable', (data => {
    
      // promptAction.showToast({
      //   message: "net is unavailable, netId is " + String(data),
      //   duration: 2000
      // });
    Logger.info(TAG, "net is unavailable, netId is " + JSON.stringify(data));
  }));
    //网络可用回调
  this.conn.on('netAvailable', (data => {
    
      // promptAction.showToast({
      //   message: "net is available, netId is " + String(data),
      //   duration: 2000
      // });
    Logger.info(TAG, "net is available, netId is " + JSON.stringify(data));
    if (this.isFirst) {
      _this.timeObj && clearTimeout(_this.timeObj)
      if (! _this.isTime) {
        _this.getStreaming()
      }
    }
    this.isFirst = true
  }))
    //网络断开回调
  this.conn.on('netLost', (data) => {
    _this.isTime = false;
    _this.startCount();
    Logger.info(TAG, 'netLost');
  })
}
