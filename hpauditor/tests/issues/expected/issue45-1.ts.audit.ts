/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue45-1.ts */
"use strict";
// It is difficult to understand the value assignment in the control judgment.
let isFoo = false;
/* HPAudit: Do not assign variables in control conditions : hp-specs-no-vars-control-condition-expns : 1 : 3 : issue45-1.ts */
isFoo = true;
if (isFoo) {
  console.log('Hello');
}

// Assignment in while loop condition.
let arr = [1, 2, 3];
/* HPAudit: Do not define multiple variables on a single line : hp-specs-no-multi-vars-single-line : 1 : 9 : issue45-1.ts */
let sum = 0;
let num = 0;
/* HPAudit: Do not assign variables in control conditions : hp-specs-no-vars-control-condition-expns : 1 : 10 : issue45-1.ts */
num = arr.pop();
while (num) {
  sum += num;
  num = arr.pop();
}
console.log(sum);

// Assignment in do loop condition.
arr = [1, 2, 3];
sum = 0;
num = arr.pop();
do {
  sum += num;
  /* HPAudit: Do not assign variables in control conditions : hp-specs-no-vars-control-condition-expns : 1 : 21 : issue45-1.ts */
  num = arr.pop();
}
while (num);
console.log(sum);
