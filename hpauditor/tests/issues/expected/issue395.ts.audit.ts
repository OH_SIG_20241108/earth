/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue395.ts */
"use strict";
// The type cannot be determined at compile time. It may be an object literal or another class Person.
/* HPAudit: Avoid use of object type aliases : hp-performance-no-type-annotation : 1 : 2 : issue395.ts */
interface Person {
  name: string;
  age: number;
};
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 7 : issue395.ts */
function greet(person: Person): number {
  return "Hello " + person.name;
}

// The type mode is not recommended, because it can be used in the following two modes, resulting in failure to determine the type at compile time.

// Invocation mode 1
class O1 {
  name: string = "";
  age: number = 0;
}
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 17 : issue395.ts */
const objectliteral: O1 = { 
    name: "zhangsan",
    age: 20
  };
/* HPAudit: Arguments must match function parameters : hp-performance-args-match-params : 1 : 18 : issue395.ts */
greet(objectliteral);

// Invocation mode 2
class Person {
  name: string = "zhangsan";
  age: number = 20;
}
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 25 : issue395.ts */
const person = new Person();
greet(person);
