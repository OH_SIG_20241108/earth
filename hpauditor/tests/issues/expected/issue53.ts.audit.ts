/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue53.ts */
"use strict";
function bar(): Promise<number> {
  return Promise.resolve(1);
}
async function foo(): Promise<number> {
  /* HPAudit: Do not use return await outside try catch : hp-specs-no-return-await : 1 : 6 : issue53.ts */
  return bar();
}
async function foo2(): Promise<number> {
  /* HPAudit: Do not use return await outside try catch : hp-specs-no-return-await : 1 : 10 : issue53.ts */
  const baz = bar();
  return baz;
}
async function foo3(): Promise<number> {
  try {
    return await bar();
  }
  catch (error) {
  }
  /* HPAudit: Do not return undefined : hp-specs-no-return-undefined : 1 : 16 : issue53.ts */
}
