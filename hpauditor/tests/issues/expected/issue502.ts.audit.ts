/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue502.ts */
"use strict";
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */
import bluetoothManager from '@ohos.bluetoothManager';
import { HiSysEventUtil } from '@ohos/common/src/main/ets/TsIndex';
import { PluginSlot } from '@ohos/common/src/main/ets/systemuicommon/plugin/info/PluginConstants';
function switchBluetooth(isOn: boolean): number {
  'use concurrent';
  try {
    if (! isOn) {
      bluetoothManager.disableBluetooth();
    } else {
      bluetoothManager.enableBluetooth();
    }
    HiSysEventUtil.reportToggleClick(PluginSlot.SLOT_STATUS_BLE, 0);
    return 0;
  }
  catch (err) {
    return err?.code;
  }
}
