/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue47.ts */
"use strict";
// Concatenates arguments.
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 2 : issue47.ts */
function concatenateAll() {
  
  // As arguments is a class array, the join method cannot be used directly.
  // You need to convert arguments to a real array first.
  /* HPAudit: Use rest parameters instead of arguments object : hp-specs-use-rest-params : 1 : 5 : issue47.ts */
  const args = Array.prototype.slice.call(arguments);
  return args.join('');
}

// Returns sum of arguments.
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 10 : issue47.ts */
function getSum(a, b, c) {
  
  // Other ways to convert arguments to an array.
  /* HPAudit: Use rest parameters instead of arguments object : hp-specs-use-rest-params : 1 : 12 : issue47.ts */
  const numbers1 = [].slice.call(arguments);
  /* HPAudit: Use rest parameters instead of arguments object : hp-specs-use-rest-params : 1 : 13 : issue47.ts */
  const numbers2 = Array.from(arguments);
  /* HPAudit: Use rest parameters instead of arguments object : hp-specs-use-rest-params : 1 : 14 : issue47.ts */
  const numbers3 = [ ... arguments];
  return numbers1.reduce( (sum, num) => sum + num, 0);
}
/* HPAudit: Arguments must match function parameters : hp-performance-args-match-params : 1 : 19 : issue47.ts */
console.log(concatenateAll('h', 'e', 'l', 'l', 'o'));
console.log(getSum(1, 2, 3));
