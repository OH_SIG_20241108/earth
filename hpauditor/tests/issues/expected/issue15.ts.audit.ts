/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue15.ts */
"use strict";
// This is an incorrect example because JS Object is used as a container to process Map logic.
class Demo {
  private getInfo(t1: string, t2: string): string {
    /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 5 : issue15.ts */
    /* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 5 : issue15.ts */
    const info: { 
        [k: string]: string
      } = {};
    this.setInfo(info);
    /* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 7 : issue15.ts */
    t1 = info[t2];
    return (t1 != null) ? t1 : '';
  }
  /* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 11 : issue15.ts */
  /* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 11 : issue15.ts */
  private setInfo(info: { 
      [k: string]: string
    }): void {
    /* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 12 : issue15.ts */
    info['T1'] = '76';
    /* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 13 : issue15.ts */
    info['T2'] = '91';
    /* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 14 : issue15.ts */
    info['T3'] = '12';
  }
  /* HPAudit: Use lowerCamelCase for variable and function names : hp-specs-lcamelcase-vars-funcs-params : 1 : 17 : issue15.ts */
  /* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 17 : issue15.ts */
  public Info(t1: string, t2: string) {
    return this.getInfo(t1, t2);
  }
}
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 22 : issue15.ts */
const myDemo = new Demo();
console.log(myDemo.Info('T1', 'T2'));
