/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue49.ts */
"use strict";
// Use dot notation to access object properties
class C {
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 3 : issue49.ts */
  objName: string = '';
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 4 : issue49.ts */
  country: string = '';
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 5 : issue49.ts */
  age: number = 0;
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 6 : issue49.ts */
  colour: string = '';
}
const obj: C = { 
    objName: 'obj',
    country: 'CA',
    age: 11,
    colour: 'red'
  }

// But not arrays
type A = number[];
const array: A = [1, 2, 3];

// Using square brackets [] to access object properties (wrong)
/* HPAudit: Use dot notation to access object properties : hp-specs-dot-notation-for-obj-props : 1 : 20 : issue49.ts */
const objName = obj['objName'];
/* HPAudit: Use dot notation to access object properties : hp-specs-dot-notation-for-obj-props : 1 : 21 : issue49.ts */
const country = obj['country'];
/* HPAudit: Use dot notation to access object properties : hp-specs-dot-notation-for-obj-props : 1 : 22 : issue49.ts */
const age = obj['age'];
console.log(objName);
console.log(country);
console.log(age);

// Using square brackets [] to access array elements (right)
const a1 = array[1];
const aa1 = array[a1];
console.log(a1);
console.log(aa1);
