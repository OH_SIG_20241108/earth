/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue04.ts */
"use strict";

// Do not access a const property in a heavy loop
function getDay(year: number): number {
  /* Year has (12 * 29 =) 348 days at least */
  let totalDays: number = 348;
  /* HPAudit: Do not access a const property in a heavy loop : hp-performance-no-const-prop-in-heavy-loop : 1 : 6 : issue04.ts */
  const T1 = Time.INFO[year - Time.START];
  for (let index: number = 0x8000; index > 0x8; index >>= 1) {
    //The INFO and START fields of Time are searched for multiple times, and the found values are the same each time.
    totalDays += ((T1 & index) !== 0) ? 1 : 0;
  }
  return totalDays + this.getDays(year);
}
