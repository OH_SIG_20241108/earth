/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue249.ts */
"use strict";
import relationalStore from '@ohos.data.relationalStore';
import { BusinessError } from '@ohos.base';
import { ValuesBucket } from '@ohos.data.ValuesBucket';
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 5 : issue249.ts */
const chatValueBuckets: ValuesBucket[]
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 7 : issue249.ts */
function upDateChatDetailData(): void {
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 9 : issue249.ts */
  const predicates = new relationalStore.RdbPredicates("CHATDETAIL");
  /* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 10 : issue249.ts */
  if (this.rdbStore !== undefined) {
    for (let i = 0; i < 1000; i++) {
      predicates.equalTo("NAME", `我是好友消息${i}`)
      /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 13 : issue249.ts */
      /* HPAudit: Avoid using closures - consider using parameters instead : hp-performance-no-closures : 1 : 13 : issue249.ts */
      const promise = (this.rdbStore as relationalStore.RdbStore).update(chatValueBuckets[i], predicates);
      promise.then(async (rows) => {
        console.log(`Updated row count: ${rows}`);
      }).catch( (err: BusinessError) => {
        console.error(`Updated failed, code is ${err.code},message is ${err.message}`);
      })
    }
  }
}
