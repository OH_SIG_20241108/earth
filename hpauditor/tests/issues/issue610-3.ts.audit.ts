/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue610-3.ts */
"use strict";
// this file must use ASCII CR-LF ('\r\n') line endings
/* HPAudit: Do not create empty objects, use literals instead : hp-performance-objs-using-literals : 1 : 2 : issue610-3.ts */
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue610-3.ts */
const arr = new Array(); // Create an array.
/* HPAudit: Do not create empty objects, use literals instead : hp-performance-objs-using-literals : 1 : 3 : issue610-3.ts */
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 3 : issue610-3.ts */
const obj = new Object(); // Create a common object.
/* HPAudit: Do not create empty objects, use literals instead : hp-performance-objs-using-literals : 1 : 5 : issue610-3.ts */
let oFruit = new Object();
oFruit.color = 'red';
oFruit.name = 'apple';
