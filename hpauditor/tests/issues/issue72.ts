// 1. There is no space before and after 'void'
// There are spaces in ${ name }
function sayHi(name1: string, name2: string, name3: string):void{
  console.log(`Hi, ${ name1 }!`);
  console.log(`Hi, ${ name2}!`);
  console.log(`Hi, ${name3 }!`);
}

// 2. There is a space between fight and (
function fight (): void {
  console.log('Swoosh!');
}

// 3. There is no space between if and (
let isJedi: boolean = true;
if(isJedi) {
  fight();
}

let isMorning: boolean = false;
if (isMorning) {
  console.log('Good morning!');
}else {  // there is no space between else and }
  console.log('Good night!');
}

// 4. There are spaces before the first element/parameter, no spaces after the comma,
// or spaces before the comma, making it difficult to read.
const arr: number[] = [ 1, 1 , 1,1,1];
const nameArr: { name: string }[] = [ { name: 'John' },{ name: 'Amy' }, { name: 'Sam' }];

sayHi('John','Amy','Sam');

// 5. There are no spaces before and after binary and ternary operators
if (0===0) {
  console.log('Zero!');
}
console.log(1+2===3);
console.log(isJedi||isMorning);

const age: number = 7;
const price: string = age >= 3?'$20':'Free';
console.log(price);
