import relationalStore from '@ohos.data.relationalStore';
import { BusinessError } from '@ohos.base';
import { ValuesBucket } from '@ohos.data.ValuesBucket';

let chatValueBuckets : ValuesBucket[]

function upDateChatDetailData() {

  let predicates = new relationalStore.RdbPredicates("CHATDETAIL");
  if(this.rdbStore != undefined) {
    for (let i = 0;i < 1000; i++) {
      predicates.equalTo("NAME", `我是好友消息${i}`)
      let promise = (this.rdbStore as relationalStore.RdbStore).update(chatValueBuckets[i], predicates);
      promise.then(async (rows) => {
         console.log(`Updated row count: ${rows}`);
      }).catch((err:BusinessError) => {
        console.error(`Updated failed, code is ${err.code},message is ${err.message}`);
      })
    }
  }
}
