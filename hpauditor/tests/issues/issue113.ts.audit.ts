/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue113.ts */
"use strict";
// Arguments must match method parameters
class C {
  static staticf(x: number, y: string): void {
    console.log(y + x);
  }
  dynamicf(x: boolean, n: number): void {
    if (x) {
      console.log(n);
    } else {
      staticf(42, '42 is ');
      /* HPAudit: Arguments must match function parameters : hp-performance-args-match-params : 1 : 11 : issue113.ts */
      staticf(); // wrong number of arguments
    }
  }
}

// Static methods
C.staticf(42, '42 is '); // ok
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 18 : issue113.ts */
C.staticf('42 is not ', 42); // wrong types
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 19 : issue113.ts */
C.staticf(42); // wrong number of arguments
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 20 : issue113.ts */
C.staticf(); // wrong number of arguments


// Dynamic methods
const o = new C();
o.dynamicf(true, 42); // ok
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 25 : issue113.ts */
o.dynamicf(42, false); // wrong types
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 26 : issue113.ts */
o.dynamicf(true); // wrong number of arguments
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 27 : issue113.ts */
o.dynamicf(); // wrong number of arguments
