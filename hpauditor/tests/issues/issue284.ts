// do not await return
async function bar() {
    return 42;
}

function ding() {
    return 0;
}

// do not await return
async function foo() {
  return await bar();       // fix this
}

// do not await return
async function foo2() {
  const baz = await bar();  // fix this
  return baz;
}

async function foo3() {
  const baz = await bar();  // do not fix this
  // real code
  const blat = baz;
  if (blat == baz) {
    // do something
  }
  // end real code
  return baz;
}

async function foo4() {
  let baz = ding();
  baz = await bar();        // fix this
  return baz;
}

async function foo5() {
  let baz = ding();
  baz = await bar();        // fix this
  return baz + 1;
}

async function foo6() {
  const baz = await bar();  // do not fix this
  if (false) {
    ///some real code here
  }
  return baz;
}

async function foo7() {
  let baz = ding();
  ///some real code here
  baz = await bar();        // fix this
  return baz;
}

async function foo8() {
  let baz = ding();
  ///some real code here
  baz = await bar();        // do not fix this
  if (false) {
    ///some real code here
  }
  return baz;
}
