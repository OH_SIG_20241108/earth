const EXCLUDE_WINDOW_NAMES = [
  // PC控制中心
  PluginSlot.SLOT_STATUS_CONTROL_CENTER,
  // PC通知中心
  PluginSlot.SLOT_STATUS_NOTIFICATION_PANEL,
  // PC服务卡片中心
  PluginSlot.SLOT_STATUS_FA_CENTER,
  // PC日历面板
  PluginSlot.SLOT_STATUS_CLOCK_PANEL,
  // PC电池面板
  PluginSlot.SLOT_STATUS_BATTERY_PANEL,
  // PC声音面板
  PluginSlot.SLOT_STATUS_SOUND_PANEL,
  // PC WiFi面板
  PluginSlot.SLOT_STATUS_WIFI_PANEL,
  // Phone下拉面板
  WindowConstants.WINDOW_NAME_DROPDOWN,
  // 锁屏窗口
  WindowConstants.WINDOW_NAME_LOCK_SCREEN
];
