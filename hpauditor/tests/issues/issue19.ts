// This is an incorrect example because some properties are added dynamically.
class O1 {
  x: string = '';
  y: string = '';
}

let obj: O1 = { 'x': 'xxx', 'y': 'yyy' };

// This dynamic addition of properties is not recommended.
obj.z = 'zzz';

// This static use of properties is allowed.
obj.y = 'yyy';

console.log(obj);
