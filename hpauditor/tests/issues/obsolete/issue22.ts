
// This is an incorrect example because variable i is expected to be an integer, but
// it is initialized as undefined/null or 0.0.

// 1. Initialize i as undefined.
function foo1(d: number): number {
  let i: undefined = undefined;
  i += d;
  return i;
}
console.log(foo1(1)); // NaN

// 2. Initialize i as null.
function foo2(d: number): number {
  let i: null = null;
  i += d;
  return i;
}
console.log(foo2(1)); // 1

// 3. Initialize i as 0.0.
function foo3(d: number): number {
  let i: number = 0.0;
  i += d;
  return i;
}
console.log(foo3(1)); // 1
