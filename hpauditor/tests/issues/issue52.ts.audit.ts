/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue52.ts */
"use strict";
// 1. Ending a finally block with return
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 2 : issue52.ts */
function foo(): string {
  try {
    throw new Error('error!');
  }
  catch (err) {
    return err.message;
  }
  finally {
    
    // this supersedes any previous return in try-catch block
    /* HPAudit: Do not end finally block abnormally : hp-specs-no-finally-end-abnormally : 1 : 9 : issue52.ts */
    return 'From finally block';
  }
}

// The incorrect value is returned.
console.log(foo()); // Outputs 'From finally block'


// 2. Ending a finally block with break
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 17 : issue52.ts */
function neverPlusOne(): number {
  for (;;) {
    try {
      return 1;
    }
    finally {
      /* HPAudit: Do not end finally block abnormally : hp-specs-no-finally-end-abnormally : 1 : 22 : issue52.ts */
      break;
    }
  }
  return - 1
}

// Always returns -1 because break in finally block overrides early return 1.
console.log(neverPlusOne());

// 3. Ending a finally block with continue
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 32 : issue52.ts */
function foo2() {
  try {
    throw new Error('Error');
  }
  catch (err) {
    return err.message;
  }
  finally {
    /* HPAudit: Do not end finally block abnormally : hp-specs-no-finally-end-abnormally : 1 : 38 : issue52.ts */
    continue; // Error thrown 
  }
}

// 4. Ending a finally block by throwing an exception
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 43 : issue52.ts */
function foo3(): void {
  try {
    throw new Error('Error1');
  }
  finally {
    /* HPAudit: Do not end finally block abnormally : hp-specs-no-finally-end-abnormally : 1 : 47 : issue52.ts */
    throw new Error('Error2');
  }
}

// Error2 is the exception shown, it supersedes Error1.
console.log(foo3());
