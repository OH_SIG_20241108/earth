function addNum(a: number, b: number): void {
    // Nested function
    function logToConsole(message: string): void {
        console.log(message);
    }

    let result = a + b;

    // Invoke the nested function.
    logToConsole("Result is " + result);
}

addNum(5, 3);
