/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue65-2.ts */
"use strict";
// Write enum members in uppercase, words separated by underscores
/* HPAudit: Name enum members in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 2 : issue65-2.ts */
enum UserType { TEACHER = 0, STUDENT = 42, PRINCIPAL_TEACHER, SCHOOL_SUPERVISOR }
let user = UserType.TEACHER;
if (user === UserType.TEACHER) {
  user = UserType.STUDENT;
  console.log(user);
}
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 16 : issue65-2.ts */
const principalTeacher = UserType.PRINCIPAL_TEACHER;
console.log(principalTeacher, UserType.SCHOOL_SUPERVISOR);
