// Write enum members in uppercase, words separated by underscores
enum UserType {
  teacher = 0,
  student = 42,
  principalTeacher,
  school_supervisor
}

let user = UserType.teacher;

if (user === UserType.teacher) {
    user = UserType.student;
    console.log (user);
}

let principalTeacher = UserType.principalTeacher;

console.log (principalTeacher, UserType.school_supervisor);
