/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue511.ts */
"use strict";
import { LogDomain, LogHelper } from '../utils/LogHelper';
import { Trace } from '../utils/Trace';
const TAG = 'GcController';
const log: LogHelper = LogHelper.getLogHelper(LogDomain.SCB, TAG);
export const DELAY_GC_TIMEOUT: number = 1000;
function triGc(reason: string): void {
  /* HPAudit: Avoid using closures - consider using parameters instead : hp-performance-no-closures : 1 : 7 : issue511.ts */
  log.showInfo(`triGc -> start forceFullGC, reason:${reason}`);
  Trace.start(`${TAG}_triGc`);
}
