//banner.et
@Component
export struct SCBBannerNotification {
  build() {
    if (! CommonUtils.isInvalid(this.ntfEntry)) {
      Column() {
        Row() {
          NotificationSingleItem({ 
            ntfEntry: this.getNtfEntry(),
            radius: ResUtils.getConvertNumber($r('app.float.ntf_item_corner_radius')),
            isInGroup: false,
            isHeadsUp: true,
            isGestureDisable: true,
            headsUpVM: this.headsUpVM
          })
            .animation(this.itemHorizontalTranAnim)
        }
          .shadow({ 
            radius: NotificationConfig.BANNER_SHADOW_RADIUS,
            color: NotificationConfig.BANNER_SHADOW_COLOR,
            offsetX: NotificationConfig.BANNER_SHADOW_OFFSET_X,
            offsetY: NotificationConfig.BANNER_SHADOW_OFFSET_Y
          })
          .margin({ 
            top: $r('app.float.headup_banner_top'),
            bottom: NotificationConfig.BANNER_MARGIN_BOTTOM,
            left: this.isLandscape ? this.bannerMarginLand : NotificationConfig.BANNER_MARGIN_LEFT_RIGHT,
            right: this.isLandscape ? this.bannerMarginLand : NotificationConfig.BANNER_MARGIN_LEFT_RIGHT
          })
          .gesture(GestureGroup(GestureMode.Parallel,
          // 横滑触摸手势
          PanGesture({ 
            direction: PanDirection.Horizontal,
            distance: NtfItemSwipeController.SWIPE_DISTANCE
          })
            .onActionCancel( () => {
              this.horizontalSwipeController?.onCancelSwipe();
              this.headsUpVM?.removeWatcher(this);
            }),
          // 横滑速度滑动
          SwipeGesture({ 
            direction: SwipeDirection.Horizontal,
            speed: NtfItemSwipeController.SWIPE_SPEED
          })
            .onAction( (event) => {
              this.horizontalSwipeController?.onSweepSpeed(event);
            })))
          .animation(this.itemHorizontalTranAnim)
      }
        .onAreaChange( (old, newArea) => {
          if (Number(newArea.width) === Number(old.width)) {
            return;
          }
          if (this.isLandscape) {
            this.bannerViewWidth = Number(newArea.width) - (DOUBLE * this.bannerMarginLand);
          } else {
            this.bannerViewWidth = Number(newArea.width) - (NotificationConfig.BANNER_MARGIN_LEFT_RIGHT * DOUBLE);
          }
          this.itemWidth = this.bannerViewWidth;
        // 设置条目宽度
          this.horizontalSwipeController?.setItemWidth(this.bannerViewWidth);
          Log.showInfo(TAG, `bannerViewWidth:${this.bannerViewWidth} newArea.width:${newArea.width}`);
          ;
        })
        .position({ 
          x: 0,
          y: 0
        })
        .translate({ 
          y: this.itemTranY
        })
      // 竖向移动过程动效
        .animation(this.itemTranAnim)
        .transition(TransitionEffect.asymmetric(TransitionEffect.translate({ 
          x: 0,
          y: - 420
        }).animation({ 
          duration: 1000,
          curve: Curves.springCurve(0, 1, 80, 14)
        }), TransitionEffect.translate({ 
          x: 0,
          y: - 420
        }).animation({ 
          duration: 600,
          curve: Curves.cubicBezierCurve(0.4, 0.0, 0.4, 1.0)
        })))
        .gesture(PanGesture({ 
          direction: PanDirection.Vertical
        }))
    }
  }
}
