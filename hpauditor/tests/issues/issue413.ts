//AppModel.ets
export enum APP_STATE {
  NOT_DOWNLOAD,  // 未下载
  DOWNLOADING,   // 下载中
  PAUSED,        // 暂停中
  WAITING,       // 等待下载中
  INSTALLING,    // 安装中
  OPEN,          // 已安装，显示open
  UPDATE,        // 已安装，显示update
}

@Observed
export class AppItem {
  title?: string;
  image?: Resource | image.PixelMap | string | null;
  iconId?: number;
  labelId?: number;
  appState?: APP_STATE;
  category?: string;
  desc?: string;
  progress?: number;
  bundleName?: string;
  moduleName?: string;
}
