/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue19.ts */
"use strict";
// This is an incorrect example because some properties are added dynamically.
class O1 {
  x: string = '';
  y: string = '';
}
let obj: O1 = { 
    'x': 'xxx',
    'y': 'yyy'
  };

// This dynamic addition of properties is not recommended.
/* HPAudit: Do not add properties dynamically : hp-performance-no-obj-props-dynamic : 1 : 10 : issue19.ts */
obj.z = 'zzz';

// This static use of properties is allowed.
obj.y = 'yyy';
console.log(obj);
