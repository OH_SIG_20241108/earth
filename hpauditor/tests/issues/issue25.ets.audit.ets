// 1. Dynamically creating a class.
// Each time foo1 is invoked, class Add and Sub are recreated, affecting memory and performance.
/* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 5 : issue25.ets */
class Add {
  operand1: number;
  operand2: number;
  constructor(operand1: number, operand2: number) {
    this.operand1 = operand1;
    this.operand2 = operand2;
  }
  add(): number {
    return this.operand1 + this.operand2;
  }
}
/* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 21 : issue25.ets */
class Sub {
  operand1: number;
  operand2: number;
  constructor(operand1: number, operand2: number) {
    this.operand1 = operand1;
    this.operand2 = operand2;
  }
  sub(): number {
    return this.operand1 - this.operand2;
  }
}
function foo1(f: boolean): number {
  if (f) {
    /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 18 : issue25.ets */
    /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 18 : issue25.ets */
    const addObj = new Add(2, 1);
    return addObj.add();
  } else {
    /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 34 : issue25.ets */
    /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 34 : issue25.ets */
    const subObj = new Sub(2, 1);
    return subObj.sub();
  }
  /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 37 : issue25.ets */
  class NestedButDependent {
    /* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 38 : issue25.ets */
    method(): boolean {
      return f;
    }
  }
}
console.log(foo1(true));
console.log(foo1(false));

// 2. Dynamically creating a function
function foo2(f: boolean, a: number, b: number): number {
  if (f) {
    /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 50 : issue25.ets */
    const add = (c: number, d: number): number => {
        return c + d;
      }
    return add(a, b);
  } else {
    /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 55 : issue25.ets */
    const sub = (e: number, g: number): number => {
        return e - g;
      }
    return sub(a, b);
  }
  /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 60 : issue25.ets */
  /* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 60 : issue25.ets */
  const nestedAndDependent: () => number = () => {
      return a + b;
    }
}
console.log(foo2(true, 2, 1));
console.log(foo2(false, 2, 1));
