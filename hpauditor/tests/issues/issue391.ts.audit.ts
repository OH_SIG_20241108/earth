/* HPAudit: Always use strict mode (fixed) : hp-specs-use-strict-mode : 1 : 0 : issue113.ts */
"use strict";
// Arguments must match method parameters
class C {
  static staticf(x: number, y: string): void {
    console.log(y + x);
  }
  dynamicf(x: boolean, n: number): void {
    if (x) {
      console.log(n);
    } else {
      staticf(42, '42 is ');
      /* HPAudit: Arguments must match function parameters : hp-performance-args-match-params : 1 : 13 : issue391.ts */
      staticf(); // wrong number of arguments
    }
  }
}

// Static methods
C.staticf(42, '42 is '); // ok
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 20 : issue391.ts */
C.staticf('42 is not ', 42); // wrong types
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 21 : issue391.ts */
C.staticf(42); // wrong number of arguments
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 22 : issue391.ts */
C.staticf(); // wrong number of arguments


// Dynamic methods
const o = new C();
o.dynamicf(true, 42); // ok
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 27 : issue391.ts */
o.dynamicf(42, false); // wrong types
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 28 : issue391.ts */
o.dynamicf(true); // wrong number of arguments
/* HPAudit: Arguments must match method parameters : hp-performance-args-match-params : 1 : 29 : issue391.ts */
o.dynamicf();
