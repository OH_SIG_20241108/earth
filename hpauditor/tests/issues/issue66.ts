let isNotError: boolean = true;
let isNotFound: boolean = false;

function noError() {
    return true;
}

function error(): boolean {
  return !isNotError;
}
function next(): boolean {
  return !isNotFound;
}
