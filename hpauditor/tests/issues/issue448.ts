export class RecentAppService {
  private context: Context;
  public static instance: RecentAppService;

  private constructor(context: Context) {
    this.context = context;
  }

  getAppItemInfosByBundleNames(bundleNames: string[]): Promise<AppItemInfo[]> {
    if (!bundleNames || !bundleNames.length) {
      return Promise.resolve<AppItemInfo[]>([]);
    }

    return Promise.all<AppItemInfo | undefined>(bundleNames.map<Promise<AppItemInfo | undefined>>(async (bundleName: string): Promise<AppItemInfo | undefined> => {
      let isInstalled = await this.isAppInstalled(bundleName)
        .catch(() => {
          return false;
        });
      if (!isInstalled) {
        return undefined;
      }

      return launcherAbilityManager.getAppInfoByBundleName(bundleName)
        .catch<undefined>((err: BusinessError): undefined => {
          LOGGER.error(RecentAppService.name, this.getAppItemInfosByBundleNames.name, err);
          return undefined;
        })
    }))
      .then((resList: (AppItemInfo | undefined)[]) => {
        return resList.filter((item: AppItemInfo | undefined) => item != undefined)
          .map((item: AppItemInfo | undefined): AppItemInfo => {
            return item as AppItemInfo;
          });
      });
  }
}
