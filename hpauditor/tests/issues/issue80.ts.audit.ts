/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue80.ts */
"use strict";
let timeoutTimes = 0;
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 2 : issue80.ts */
function timeout(): void {
  timeoutTimes++;
  if (timeoutTimes < 10) {
    setTimeout(timeout, 10);
  }
}
timeout();
