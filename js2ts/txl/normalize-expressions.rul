% JS2TS - convert JavaScript to TypeScript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% Normalization of expressions to remove notational variants for the same expression
% and assist transformation

function normalizeExpressions
    replace [program]
        P [program]
    by
        P [_normalizeBigInts]
          [_normalizeNewConstructors]
          [_normalizeArrayLiterals]
          [_normalizeArrayConstructors]
end function

function unnormalizeExpressions
    replace [program]
        P [program]
    by
        P [_unnormalizeBigInts]
end function

rule _normalizeBigInts
    % Bigint literals must be parsed as [number]s since otherwise
    % they conflict with the [number] token pattern.
    % In order to make them more easily recognizable in type inference patterns,
    % we convert them to type converter notation - e.g., 42n => bigint("42")
    replace $ [MemberExpression]
        N [NumericLiteral]
    % A bigint literal has an "n" as its last character
    construct Nstring [stringlit]
        _ [unparse N]
    construct LengthN [number]
        _ [# Nstring]
    construct Nlast [stringlit]
        Nstring [: LengthN LengthN]
    deconstruct Nlast
        "n"
    % Remove that final "n"
    construct LengthNm1 [number]
        LengthN [- 1]
    construct NstringM1 [stringlit]
        Nstring [: 1 LengthNm1]
    % And wrap it in bigint()
    by
        'bigint '( NstringM1 ')
end rule

rule _unnormalizeBigInts
    % Of course, on output we'd rather use the standard bigint literal notation,
    % so undo the conversion above on output
    replace $ [MemberExpression]
        'bigint '( BigIntString [stringlit] ')
    % Add the trailing "n" back
    construct BigIntNumberString [stringlit]
        BigIntString [+ "n"]
    % And reparse it as a bigint [number]
    construct OptBigIntNumber [opt NumericLiteral]
        _ [pragma "--newline"] [parse BigIntNumberString]
    deconstruct OptBigIntNumber
        BigIntNumber [NumericLiteral]
    by
        BigIntNumber
end rule

rule _normalizeArrayLiterals
    % Array constructors can use either [1,2,3] notation or new Array(1,2,3) notation
    replace $ [MemberExpression]
        'new 'Array '( Arguments [list AssignmentExpressionOrSpreadArgument+] ')
    % Ooops! A single argument Array() means the size of the array, not its elements
    deconstruct not Arguments
        SingleArgument [AssignmentExpression]
    % We normalize to [1,2,3] notation
    construct Elements [list Element]
        _ [_convertArgumentToElement each Arguments]
    by
        '[ Elements ']
end rule

function _convertArgumentToElement Argument [AssignmentExpressionOrSpreadArgument]
    % Convert argument expressions to element expressions
    deconstruct Argument
        OptSpread [Spread?] Expression [AssignmentExpression]
    replace * [list Element]
        % empty, i.e., add to the end of the list
    by
        OptSpread Expression
end function

rule _normalizeArrayConstructors
    % Single argument Array() means the pre-allocated size of the array
    replace $ [MemberExpression]
        'new 'Array '( SingleArgument [AssignmentExpression] ')
    % We simply normalize it as a generic array
    by
        '[ ']
end rule

rule _normalizeNewConstructors
    % "new" can be used with or without arguments, if there are none
    replace $ [MemberExpression]
        'new  BaseMemberExpression [BaseMemberExpression]
    % Normalize to always have empty arguments
    by
        'new BaseMemberExpression '( ')
end rule
