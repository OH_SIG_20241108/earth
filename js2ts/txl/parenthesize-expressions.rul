% JS2TS - convert JavaScript to TypeScript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% Parenthesize expressions

% We begin by parenthesizing all operator expressions to allow for type annotations of intermediate results.
% For example, the expression x + y becomes (x + y) so that, when annotated with types, we can annotate
% all of x, y, and the result x+y with their inferred types, e.g., ( x (: number) + y (: string) ) (: string)

% Grammatically, expression type annotations can only be attached to [PrimaryExpression]s.
% Expressions without operators are already [PrimaryExpression]s, so only expressions with operators
% need to be parenthesized. When we're done, every expression and sub-expression is a [PrimaryExpression].

function parenthesizeExpressions
    replace [program]
        P [program]
    by
        P [_parenthesizeExpressions]
end function

function unparenthesizeExpressions
    replace [program]
        P [program]
    by
        P [_unparenthesizeExpressions]
          [_reparenthesizeConversionExpressions]
end function

% Private

function _parenthesizeExpressions
    % Because we have a precedence grammar that correctly groups operands with their operators
    % according to the semantics of JS/TS, we must parenthesize each operator precedence level separately,
    % and in the order lowest precedence (assignment) to highest precedence (subscripts and field selections)

    % The best way to do this in TXL is using one-pass rules recursively to visit each operator expression
    % from the inside out.
    replace [any]
        Scope [any]
    by
        Scope [_parenthesizeParenthesizedExpressions]
              [_parenthesizeAssignmentExpressions]
              [_parenthesizeConditionalExpressions]
              [_parenthesizeCoalesceExpressions]
              [_parenthesizeLogicalORExpressions]
              [_parenthesizeLogicalANDExpressions]
              [_parenthesizeBitwiseORExpressions]
              [_parenthesizeBitwiseXORExpressions]
              [_parenthesizeBitwiseANDExpressions]
              [_parenthesizeEqualityExpressions]
              [_parenthesizeRelationalExpressions]
              [_parenthesizeShiftExpressions]
              [_parenthesizeAdditiveExpressions]
              [_parenthesizeMultiplicativeExpressions]
              [_parenthesizeExponentiationExpressions]
              [_parenthesizeUnaryExpressions]
              [_parenthesizeUpdateExpressions]
              [_parenthesizeNewExpressions]
              [_parenthesizeArgumentExpressions]
              [_parenthesizeSubscriptExpressions]
              [_parenthesizeDotExpressions]
              [_parenthesizeReturnExpressions]
              [_parenthesizeMultipleAssignmentExpressions]
end function

function _unparenthesizeExpressions
    % Unparenthesizing is basically the same process in reverse,
    % but is simpler since we don't have to match operations
    replace [any]
        Scope [any]

    by
        Scope [_unparenthesizeMultipleAssignmentExpressions]
              [_unparenthesizeReturnExpressions]
              [_unparenthesizeMemberExpressions]
              [_unparenthesizeNewExpressions]
              [_unparenthesizeUpdateExpressions]
              [_unparenthesizeUnaryExpressions]
              [_unparenthesizeExponentiationExpressions]
              [_unparenthesizeMultiplicativeExpressions]
              [_unparenthesizeAdditiveExpressions]
              [_unparenthesizeShiftExpressions]
              [_unparenthesizeRelationalExpressions]
              [_unparenthesizeEqualityExpressions]
              [_unparenthesizeBitwiseANDExpressions]
              [_unparenthesizeBitwiseXORExpressions]
              [_unparenthesizeBitwiseORExpressions]
              [_unparenthesizeLogicalANDExpressions]
              [_unparenthesizeLogicalORExpressions]
              [_unparenthesizeCoalesceExpressions]
              [_unparenthesizeConditionalExpressions]
              [_unparenthesizeAssignmentExpressions]
end function

% Private

% Parenthesizing rules

% In order to terminate, we skip already parenthesized expressions in these rules.
% But since parenthesized expressions may already exist in the original code,
% we must parenthesize inside those inner expressions first.

rule _parenthesizeParenthesizedExpressions
    skipping [ParenthesizedExpression]
    replace $ [ParenthesizedExpression]
        '( Expression [Expression] ')
    deconstruct not Expression
        _ [ParenthesizedExpression]
    by
        '( Expression [_parenthesizeExpressions] ')
end rule

% The remaining parenthesizing rules are simply one rule per precedence level in the grammar,
% matching only those expressions that have an operator.

rule _parenthesizeAssignmentExpressions
    skipping [ParenthesizedExpression]
    replace [AssignmentExpression]
        LeftHandSideExpression [LeftHandSideExpression]
            AssignmentOp [AssignmentOp] AssignmentExpression [AssignmentExpression]
    by
        '( LeftHandSideExpression [_parenthesizeExpressions]
           AssignmentOp AssignmentExpression [_parenthesizeExpressions] ')
end rule

rule _parenthesizeConditionalExpressions
    skipping [ParenthesizedExpression]
    replace [ConditionalExpression]
        LogicalORExpression [LogicalORExpression]
            ConditionalOpAssignmentExpression [ConditionalOpAssignmentExpression]
    by
        '( LogicalORExpression [_parenthesizeExpressions]
           ConditionalOpAssignmentExpression [_parenthesizeExpressions] ')
end rule

rule _parenthesizeCoalesceExpressions
    skipping [ParenthesizedExpression]
    replace [CoalesceExpression]
        CoalesceExpression [CoalesceExpression]
            CoalesceOpBitwiseORExpression [CoalesceOpBitwiseORExpression]
    construct ParenthesizedExpression [PrimaryExpression]
        '( CoalesceExpression [_parenthesizeExpressions]
            CoalesceOpBitwiseORExpression [_parenthesizeExpressions] ')
    by
        ParenthesizedExpression
end rule

rule _parenthesizeLogicalORExpressions
    skipping [ParenthesizedExpression]
    replace [LogicalORExpression]
        LogicalANDExpression [LogicalANDExpression]
            LogicalORLogicalANDExpression [LogicalORLogicalANDExpression]
    by
        '( LogicalANDExpression [_parenthesizeExpressions]
            LogicalORLogicalANDExpression [_parenthesizeExpressions] ')
end rule

rule _parenthesizeLogicalANDExpressions
    skipping [ParenthesizedExpression]
    replace [LogicalANDExpression]
        LogicalANDExpression [LogicalANDExpression]
            LogicalANDCoalesceExpression [LogicalANDCoalesceExpression]
    construct PrimaryExpression [PrimaryExpression]
        '( LogicalANDExpression [_parenthesizeExpressions]
            LogicalANDCoalesceExpression [_parenthesizeExpressions] ')
    by
        PrimaryExpression
end rule

rule _parenthesizeBitwiseORExpressions
    skipping [ParenthesizedExpression]
    replace [BitwiseORExpression]
        BitwiseXORExpression [BitwiseXORExpression]
            BitwiseORBitwiseXORExpression [BitwiseORBitwiseXORExpression]
    by
        '( BitwiseXORExpression [_parenthesizeExpressions]
            BitwiseORBitwiseXORExpression [_parenthesizeExpressions] ')
end rule

rule _parenthesizeBitwiseXORExpressions
    skipping [ParenthesizedExpression]
    replace [BitwiseXORExpression]
        BitwiseANDExpression [BitwiseANDExpression]
            BitwiseXORBitwiseANDExpression [BitwiseXORBitwiseANDExpression]
    by
        '( BitwiseANDExpression [_parenthesizeExpressions]
            BitwiseXORBitwiseANDExpression [_parenthesizeExpressions] ')
end rule

rule _parenthesizeBitwiseANDExpressions
    skipping [ParenthesizedExpression]
    replace [BitwiseANDExpression]
        EqualityExpression [EqualityExpression]
            BitwiseANDEqualityExpression [BitwiseANDEqualityExpression]
    by
        '( EqualityExpression [_parenthesizeExpressions]
            BitwiseANDEqualityExpression [_parenthesizeExpressions] ')
end rule

rule _parenthesizeEqualityExpressions
    skipping [ParenthesizedExpression]
    replace [EqualityExpression]
        EqualityExpression [EqualityExpression]
            EqualityOpRelationalExpression [EqualityOpRelationalExpression]
    construct PrimaryExpression [PrimaryExpression]
        '( EqualityExpression [_parenthesizeExpressions]
           EqualityOpRelationalExpression [_parenthesizeExpressions] ')
    by
        PrimaryExpression
end rule

rule _parenthesizeRelationalExpressions
    skipping [ParenthesizedExpression]
    replace [RelationalExpression]
        RelationalExpression [RelationalExpression]
            RelationalOpShiftExpression [RelationalOpShiftExpression]
    construct PrimaryExpression [PrimaryExpression]
        '( RelationalExpression [_parenthesizeExpressions]
           RelationalOpShiftExpression [_parenthesizeExpressions] ')
    by
        PrimaryExpression
end rule

rule _parenthesizeShiftExpressions
    skipping [ParenthesizedExpression]
    replace [ShiftExpression]
        AdditiveExpression [AdditiveExpression]
           ShiftOpAdditiveExpression [ShiftOpAdditiveExpression]
    by
        '( AdditiveExpression [_parenthesizeExpressions]
           ShiftOpAdditiveExpression [_parenthesizeExpressions] ')
end rule

rule _parenthesizeAdditiveExpressions
    skipping [ParenthesizedExpression]
    replace [AdditiveExpression]
        MultiplicativeExpression [MultiplicativeExpression]
           AdditiveOpMultiplicativeExpression [AdditiveOpMultiplicativeExpression]
    by
        '( MultiplicativeExpression [_parenthesizeExpressions]
           AdditiveOpMultiplicativeExpression [_parenthesizeExpressions] ')
end rule

rule _parenthesizeMultiplicativeExpressions
    skipping [ParenthesizedExpression]
    replace [MultiplicativeExpression]
        ExponentiationExpression [ExponentiationExpression]
           MultiplicativeOpExponentiationExpression [MultiplicativeOpExponentiationExpression]
    by
        '( ExponentiationExpression [_parenthesizeExpressions]
           MultiplicativeOpExponentiationExpression [_parenthesizeExpressions] ')
end rule

rule _parenthesizeExponentiationExpressions
    skipping [ParenthesizedExpression]
    replace [ExponentiationExpression]
        UpdateExpression [UpdateExpression]
           ExponentOpExponentiationExpression [ExponentOpExponentiationExpression]
    by
        '( UpdateExpression [_parenthesizeExpressions]
           ExponentOpExponentiationExpression [_parenthesizeExpressions] ')
end rule

rule _parenthesizeUnaryExpressions
    skipping [ParenthesizedExpression]
    replace [UnaryExpression]
        PrefixOp [PrefixOp] UpdateExpression [UpdateExpression]
    by
        '( PrefixOp UpdateExpression [_parenthesizeExpressions] ')
end rule

rule _parenthesizeUpdateExpressions
    skipping [ParenthesizedExpression]
    replace $ [UpdateExpression]
        UpdateExpression [UpdateExpression]
    by
        UpdateExpression [_parenthesizePreUpdateExpressions]
                         [_parenthesizePostUpdateExpressions]
end rule

function _parenthesizePreUpdateExpressions
    replace [UpdateExpression]
        UpdateOp [UpdateOp] MemberExpression [MemberExpression]
    by
        '( UpdateOp MemberExpression [_parenthesizeExpressions] ')
end function

function _parenthesizePostUpdateExpressions
    replace [UpdateExpression]
        MemberExpression [MemberExpression] UpdateOp [UpdateOp]
    by
        '( MemberExpression [_parenthesizeExpressions] UpdateOp ')
end function

% Function calls with arguments, subscripts and field selections all count as operations since
% they yield result types that may differ from their parts.

rule _parenthesizeArgumentExpressions
    skipping [ParenthesizedExpression]
    replace [MemberExpression]
        BaseMemberExpression [BaseMemberExpression] Arguments [Arguments] MemberComponents [MemberComponent*]
    skipping [ObjectLiteral]
    deconstruct not * [FunctionDeclaration] BaseMemberExpression
        _ [FunctionDeclaration]
    by
        '( BaseMemberExpression Arguments [_parenthesizeExpressions] ') MemberComponents
end rule

rule _parenthesizeSubscriptExpressions
    skipping [ParenthesizedExpression]
    replace [MemberExpression]
        BaseMemberExpression [BaseMemberExpression] '[ Expression [Expression] '] MemberComponents [MemberComponent*]
    by
        '( BaseMemberExpression '[ Expression [_parenthesizeExpressions] '] ') MemberComponents
end rule

rule _parenthesizeDotExpressions
    skipping [ParenthesizedExpression]
    replace [MemberExpression]
        BaseMemberExpression [BaseMemberExpression] '. FieldIdentifier [FieldIdentifier] MemberComponents [MemberComponent*]
    by
        '( BaseMemberExpression '. FieldIdentifier ') MemberComponents
end rule

rule _parenthesizeNewExpressions
    skipping [ParenthesizedExpression]
    replace [MemberExpression]
        'new BaseMemberExpression [BaseMemberExpression] MemberComponents [MemberComponent*]
    by
        '( 'new BaseMemberExpression MemberComponents ')
end rule

rule _parenthesizeReturnExpressions
    replace $ [ReturnStatement]
        'return Expression [Expression] End [EOS]
    deconstruct not Expression
        '( _ [Expression] ')
    by
        'return '( Expression ') End
end rule

% Multiple assignments are actually assignments of the results of assignment expressions.
% We parenthesize them separately in case their types may have to be different.

rule _parenthesizeMultipleAssignmentExpressions
    replace [AssignmentExpression]
        LeftHandSideExpression1 [LeftHandSideExpression] AssignmentOp1 [AssignmentOp]
            LeftHandSideExpression2 [LeftHandSideExpression] AssignmentOp2 [AssignmentOp] AssignmentExpression [AssignmentExpression]
    by
        LeftHandSideExpression1 AssignmentOp1 '( LeftHandSideExpression2 AssignmentOp2 AssignmentExpression ')
end rule

% Unparenthesizing rules

% These rules basically do the same thing in reverse, using the same one-pass recursive technique
% from the highest precedence to the lowest, again inside out.
% They are simpler since we don't need to check for operations.

rule _unparenthesizeMultipleAssignmentExpressions
    replace $ [AssignmentExpression]
        LeftHandSideExpression1 [LeftHandSideExpression] AssignmentOp1 [AssignmentOp]
            '( LeftHandSideExpression2 [LeftHandSideExpression] AssignmentOp2 [AssignmentOp] AssignmentExpression [AssignmentExpression] ')
    by
        LeftHandSideExpression1 [_unparenthesizeExpressions] AssignmentOp1 LeftHandSideExpression2 [_unparenthesizeExpressions]
            AssignmentOp2 AssignmentExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeReturnExpressions
    replace $ [ReturnStatement]
        'return '( Expression [Expression] ') End [EOS]
    by
        'return Expression [_unparenthesizeExpressions] End
end rule

rule _unparenthesizeMemberExpressions
    % Handles arguments, subscripts and field selections
    replace [MemberExpression]
        '( BaseMemberExpression [BaseMemberExpression] MemberComponents1 [MemberComponent*] ') MemberComponents2 [MemberComponent*]
    by
        BaseMemberExpression [_unparenthesizeExpressions] MemberComponents1 [. MemberComponents2]
end rule

rule _unparenthesizeNewExpressions
    replace $ [MemberExpression]
        '( 'new MemberExpression [MemberExpression] ') MemberComponents [MemberComponents]
    by
        'new MemberExpression [_unparenthesizeExpressions] MemberComponents
end rule

rule _unparenthesizeUpdateExpressions
    skipping [ParenthesizedExpression]
    replace $ [UpdateExpression]
        '( UpdateExpression [UpdateExpression] ')
    by
        UpdateExpression
end rule

rule _unparenthesizeUnaryExpressions
    replace $ [UnaryExpression]
        '( UnaryExpression [UnaryExpression] ')
    by
        UnaryExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeExponentiationExpressions
    replace $ [ExponentiationExpression]
        '( ExponentiationExpression [ExponentiationExpression] ')
    by
        ExponentiationExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeMultiplicativeExpressions
    replace $ [MultiplicativeExpression]
        '( MultiplicativeExpression [MultiplicativeExpression] ')
    by
        MultiplicativeExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeAdditiveExpressions
    replace $ [AdditiveExpression]
        '( AdditiveExpression [AdditiveExpression] ')
    by
        AdditiveExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeShiftExpressions
    replace $ [ShiftExpression]
        '( ShiftExpression [ShiftExpression] ')
    by
        ShiftExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeRelationalExpressions
    replace $ [RelationalExpression]
        '( RelationalExpression [RelationalExpression] ')
    by
        RelationalExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeEqualityExpressions
    replace $ [EqualityExpression]
        '( EqualityExpression [EqualityExpression] ')
    by
        EqualityExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeBitwiseANDExpressions
    replace $ [BitwiseANDExpression]
        '( BitwiseANDExpression [BitwiseANDExpression] ')
    by
        BitwiseANDExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeBitwiseXORExpressions
    replace $ [BitwiseXORExpression]
        '( BitwiseXORExpression [BitwiseXORExpression] ')
    by
        BitwiseXORExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeBitwiseORExpressions
    replace $ [BitwiseORExpression]
        '( BitwiseORExpression [BitwiseORExpression] ')
    by
        BitwiseORExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeLogicalANDExpressions
    replace $ [LogicalANDExpression]
        '( LogicalANDExpression [LogicalANDExpression] ')
    by
        LogicalANDExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeLogicalORExpressions
    replace $ [LogicalORExpression]
        '( LogicalORExpression [LogicalORExpression] ')
    by
        LogicalORExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeCoalesceExpressions
    replace $ [CoalesceExpression]
        '( CoalesceExpression [CoalesceExpression] ')
    by
        CoalesceExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeConditionalExpressions
    replace $ [ConditionalExpression]
        '( ConditionalExpression [ConditionalExpression] ')
    by
        ConditionalExpression [_unparenthesizeExpressions]
end rule

rule _unparenthesizeAssignmentExpressions
    replace $ [AssignmentExpression]
        '( LeftHandSideExpression [LeftHandSideExpression]
            AssignmentOp [AssignmentOp] AssignmentExpression [AssignmentExpression] ')
    by
        LeftHandSideExpression [_unparenthesizeExpressions] AssignmentOp AssignmentExpression [_unparenthesizeExpressions]
end rule

% Reparenthesize function expressions and type conversions for more readable output

function _reparenthesizeConversionExpressions
    replace [any]
        Scope [any]
    by
        Scope [_reparenthesizeNumericTypeConversions]
              [_reparenthesizeAnyTypeConversions]
              [_reparenthesizeFunctionExpressions]
              [_reparenthesizeDeconstructingAssignments]
              [_reparenthesizeObjectLiteralStatements]
              [_reparenthesizeNegatedLeftOperands]
end function

rule _reparenthesizeNumericTypeConversions
    skipping [ParenthesizedExpression]
    replace $ [UnaryExpression]
        '+ UpdateExpression [UpdateExpression]
    % If it's a simple +1 then don't parenthesize it
    deconstruct not UpdateExpression
        _ [Literal]
    by
        '( '+ UpdateExpression ')
end rule

rule _reparenthesizeAnyTypeConversions
    skipping [ParenthesizedExpression]
    replace $ [UnaryExpression]
        PrefixExpression [PrefixExpression] 'as 'any
    by
        '( PrefixExpression 'as 'any ')
end rule

% Function expressions must be parenthesized in TS

rule _reparenthesizeFunctionExpressions
    skipping [ParenthesizedExpression]
    replace $ [MemberExpression]
        FunctionExpression [FunctionExpression] MemberComponents [MemberComponent+]
    by
        '( FunctionExpression [_reparenthesizeFunctionExpressions] MemberComponents ')
end rule

% Deconstructing assignment statements must be parenthesized in TS

rule _reparenthesizeDeconstructingAssignments
    replace $ [ExpressionStatement]
        DeconstructingObjectLiteral [ObjectLiteral] AssignmentOp [AssignmentOp] AssignmentExpression [AssignmentExpression] End [EOS]
    by
        '( DeconstructingObjectLiteral AssignmentOp AssignmentExpression ') End
end rule

% Object literal expression statements must be parenthesized in TS

rule _reparenthesizeObjectLiteralStatements
    replace $ [ExpressionStatement]
        ConditionalExpression [ConditionalExpression] End [EOS]
    skipping [Arguments] [PrimaryExpression]
    deconstruct * [PrimaryExpression] ConditionalExpression
        ObjectLiteral [ObjectLiteral]
    by
        ConditionalExpression [_reparenthesizeObjectLiteral] End
end rule

function _reparenthesizeObjectLiteral
    skipping [Arguments] [PrimaryExpression]
    replace * [PrimaryExpression]
        ObjectLiteral [ObjectLiteral]
    by
        '( ObjectLiteral ')
end function

% Negated left operands must be parenthesized in TS
rule _reparenthesizeNegatedLeftOperands
    replace $ [ExponentiationExpression]
        '- UpdateExpression [UpdateExpression] '** ExponentiationExpression [ExponentiationExpression]
    by
        '( '- UpdateExpression ') '** ExponentiationExpression
end rule 
