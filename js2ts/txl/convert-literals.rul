% JS2TS - convert JavaScript to TypeScript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% Convert JS literals to TS

function convertLiterals
    replace [program]
        P [program]
    by
        P [_convertOctalLiterals]
          [_convertErroneousOctalLiterals]
end function

% Private

rule _convertOctalLiterals
    % Convert JS octal numbers to TS-style octal numbers, e.g., 0NNN => 0oNNN
    replace $ [PrimaryExpression]
        Number [number]
    % Is it a JS octal number (beginning with 0)?
    construct NumberString [stringlit]
        _ [+ Number]
    construct Zero [stringlit]
        NumberString [: 1 1]
    deconstruct Zero
        "0"
    % Make sure that it has digits in it (otherwise it's just 0)
    construct Rest [stringlit]
        NumberString [: 2 999]
    deconstruct not Rest
        ""
    deconstruct not Rest
        "n"
    % Is it well formed (if not, it's a malformed decimal number)?
    where not
        Rest [grep "8"] [grep "9"]
             [grep "."] [grep "e"] [grep "E"]
    % Yes, so convert it to TS notation and reparse it to an octal number
    construct OctalNumberString [stringlit]
        _ [+ "0o"] [+ Rest]
    construct OctalNumber [octal]
        _ [pragma "--newline"] [parse OctalNumberString]
    by
        OctalNumber
end rule

rule _convertErroneousOctalLiterals
    % Convert erroneous JS decimal numbers beginning with 0, e.g., 0NNN => NNN
    replace $ [PrimaryExpression]
        Number [number]
    % Is it a JS octal number (beginning with 0)?
    construct NumberString [stringlit]
        _ [+ Number]
    construct Zero [stringlit]
        NumberString [: 1 1]
    deconstruct Zero
        "0"
    % Make sure that it has digits in it (otherwise it's just 0)
    construct Rest [stringlit]
        NumberString [: 2 999]
    deconstruct not Rest
        ""
    deconstruct not Rest
        "n"
    % Is it not really octal?
    where
        Rest [grep "8"] [grep "9"]
    % Then it's a decimal number with leading 0, illegal in TS, so reparse it without the 0
    construct DecimalNumber [number]
        _ [pragma "--newline"] [parse Rest] [+ 0]
    by
        DecimalNumber
end rule
