% JS2TS - convert JavaScript to TypeScript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% Variable type propagation rules

% If we know the type of a variable, we can infer the expression type of references to it, and vice versa

% Main variable type inference rule
function propagateVariableTypes
    replace [program]
        P [program]
    by
        P [_propagateVariableTypesRightLeft]
          [_propagateVariableTypesLeftRight]
end function

% Private

rule _propagateVariableTypesRightLeft
    % Infer the unknown type of a variable from the known type of a reference expression use of it
    % Unique naming guarantees no name conflicts
    replace $ [SourceElement*]
        LetOrConst [LetOrConst] Id [id] Nullable [Nullable ?] ': 'any Initializer [Initializer?] End [EOS]
        Scope [SourceElement*]
    % Do we have a reference to it of known type?
    deconstruct * [PrimaryExpression] Scope
        Id '(: Type [KnownType] )
    % If so, infer the type of the variable from the expression type
    by
        LetOrConst Id Nullable ': Type Initializer End
        Scope [_propagateVariableType Id Type]
end rule

rule _propagateVariableTypesLeftRight
    % Infer the type of unknown references to a variable of known type from the variable type
    % Do we have a variable declaration of known type?
    replace $ [SourceElement*]
        LetOrConst [LetOrConst] Id [id] Nullable [Nullable ?] ': Type [KnownType] Initializer [Initializer?] End [EOS]
        Scope [SourceElement*]
    % If so, set the type of unknown references to it
    by
        LetOrConst Id Nullable ': Type Initializer End
        Scope [_propagateVariableType Id Type]
end rule

rule _propagateVariableType Id [id] Type [KnownType]
    replace $ [PrimaryExpression]
        Id '(: 'any )
    by
        Id '(: Type )
end rule
