JS to TS Standalone Tool Build Process
J.R. Cordy, Huawei Technologies

September 2023 (Rev Sep 2023)

This is the build process for compiling and building the distributable standalone js2ts tool.
Two versions are built, bin/js2ts for single file use, and bin/js2tsmf for use with the multifile process.

To build both versions, use the command:
	make
in this directory.

To run the compiled js2ts tool, copy bin/js2ts and bin/js2tsmf to any directory
on your command path, and use it as follows:

    js2ts input.js > output.js.ts

TXL is not required to run the standalone js2ts tools, so they can be distributed independently.
No debugging options are supported in the standalone js2ts tools.

JRC 19.9.23
