Prototype JS to TS Process Using Type Inference
J.R. Cordy, Huawei Technologies

June 2022 (Revised August 2023)

This is a rapid prototype of the planned JS to TS process using type inference in TXL. 

To run the JS to TS process on an input, use the command:
	txl app.js js2ts.txl > app.js.ts
where "app.js" is the input JS source file to be converted and "app.js.ts" is the TS output.

The process is not yet tuned for time and space, so for large inputs (thousands of input lines), 
at present it is recommended to use a larger TXL size.
	txl bigapp.js -s 1000 js2ts.txl > bigapp.js.ts

The following debugging options are supported:
    -d VERBOSE          Log steps of the process and inference iterations on the terminal.
    -d TRACE            Show the results of each step of the process on the terminal (for small inputs).
    -d EXPNTYPES        Retain inferred expression type annotations in the output (not pure TS).

The following style options are supported:
    -d NOUNIONS         Do not use union types (use 'any' instead).
    -d TSSTYLE          Standard TS style (add only required type annotations, do not use union types)

JRC 10.8.23
