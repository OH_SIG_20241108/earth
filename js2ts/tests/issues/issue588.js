var parseTwoDigitYear = function (input) {
    return input;
};
var o = {
    parseTwoDigitYear: function (input) {
        input = +input;
        return input + (input > 68 ? 1900 : 2000);
    }
};
if (o && o.parseTwoDigitYear) {
    (parseTwoDigitYear = o.parseTwoDigitYear);
}
console.log(parseTwoDigitYear(1));
