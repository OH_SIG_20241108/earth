// vars
t = true;
f = false;
s = "abc";
n = 42;
b = 42n;

// boolean
let b1 = t && f;
console.log (typeof (t && f));
console.log (t && f);
let b2 = f && t;
console.log (typeof (f && t));
console.log (f && t);
let b3 = t && s;
console.log (typeof (t && s));
console.log (t && s);
let b4 = f && s;
console.log (typeof (f && s));
console.log (f && s);
let b5 = s && t;
console.log (typeof (s && t));
console.log (s && t);
let b6 = s && f;
console.log (typeof (s && f));
console.log (s && f);
let b7 = t && n;
console.log (typeof (t && n));
console.log (t && n);
let b8 = f && n;
console.log (typeof (f && n));
console.log (f && n);
let b9 = n && t;
console.log (typeof (n && t));
console.log (n && t);
let b10 = n && f;
console.log (typeof (n && f));
console.log (n && f);
let b11 = t && b;
console.log (typeof (t && b));
console.log (t && b);
let b12 = f && b;
console.log (typeof (f && b));
console.log (f && b);
let b13 = b && t;
console.log (typeof (b && t));
console.log (b && t);
let b14 = b && f;
console.log (typeof (b && f));
console.log (b && f);
let b15 = t && undefined;
console.log (typeof (t && undefined));
console.log (t && undefined);
let b16 = f && undefined;
console.log (typeof (f && undefined));
console.log (f && undefined);
let b17 = undefined && t;
console.log (typeof (undefined && t));
console.log (undefined && t);
let b18 = undefined && f;
console.log (typeof (undefined && f));
console.log (undefined && f);

// string
let s1 = s && s;
console.log (typeof (s && s));
console.log (s && s);
let s2 = s && n;
console.log (typeof (s && n));
console.log (s && n);
let s3 = n && s;
console.log (typeof (n && s));
console.log (n && s);
let s4 = s && b;
console.log (typeof (s && b));
console.log (s && b);
let s5 = b && s;
console.log (typeof (b && s));
console.log (b && s);
let s6 = s && undefined;
console.log (typeof (s && undefined));
console.log (s && undefined);
let s7 = undefined && s;
console.log (typeof (undefined && s));
console.log (undefined && s);

// number
let n1 = n && n;
console.log (typeof (n && n));
console.log (n && n);
let n2 = n && b;
console.log (typeof (n && b));
console.log (n && b);
let n3 = b && n;
console.log (typeof (b && n));
console.log (b && n);
let n4 = n && undefined;
console.log (typeof (n && undefined));
console.log (n && undefined);
let n5 = undefined && n;
console.log (typeof (undefined && n));
console.log (undefined && n);

// random
c = [1, "a"] && 123;
console.log (c);
d = t && undefined;
console.log (d);

// objects
objectx = {};
objecty = {};
objectx.prop = t;
objecty.prop = 1.1;
v1 = objectx.prop;
v2 = objecty.prop;
v = v1 && v2;
console.log (v);
