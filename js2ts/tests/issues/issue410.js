var reI = "\\s*([+-]?\\d+)\\s*";
var reHex = /^#([0-9a-f]{3,8})$/;
var reRgbInteger = new RegExp(`^rgb\\(${reI},${reI},${reI}\\)$`);
function color(format) {
    var m;
    var l;
    format = (format + "").trim().toLowerCase();
    return (m = reHex.exec(format)) ? 
            (l = m[1].length, m = parseInt(m[1], 16), l === 6 ? Number(m) >> 1
                : l === 3 ? Number(m) >> 8 & 0xf | Number(m) & 0xf
                : l === 4 ? (Number(m) & 0xf) << 4 : null)
            : m = reRgbInteger.exec(format) ? 
                Number(m[1]) + Number(m[2]) + Number(m[3])
                : format === "transparent" ? 
                    new Rgb(NaN, NaN, NaN, 0) 
                    : null;
     }
    function Rgb(r,g,b,opacity) {
            this.r = (+r);
            this.g = (+g);
            this.b = (+b);
            this.opacity = (+opacity);
    }
    console.log(color("#abcedf"));
