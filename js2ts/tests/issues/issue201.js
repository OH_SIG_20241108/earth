class Employee {
    static headcount = 0;

    static personcount () {
	return headcount;
    }

    constructor(firstName, lastName, jobTitle) {
        Employee.headcount++;
    }
}

let john = new Employee('John', 'Doe', 'Front-end Developer');
let jane = new Employee('Jane', 'Doe', 'Back-end Developer');

let headcount = Employee.headcount;
console.log(headcount); 

let personcount = Employee.personcount();
console.log(personcount); 

console.log (headcount == personcount);
