// Copyright (C) 2016 the V8 project authors. All rights reserved.
// This code is governed by the BSD license found in the LICENSE file.
/*---
esid: sec-module-namespace-exotic-objects-ownpropertykeys
description: >
    The [[OwnPropertyKeys]] internal method reflects the sorted order
info: |
    1. Let exports be a copy of the value of O's [[Exports]] internal slot.
    2. Let symbolKeys be ! OrdinaryOwnPropertyKeys(O).
    3. Append all the entries of symbolKeys to the end of exports.
    4. Return exports.
flags: [module]
features: [Reflect, Symbol.toStringTag]
---*/

var x;
var π = x; // u03c0
var az = x;
var __ = x;
var za = x;
var Z = x;
var \u03bc = x;
var z = x;
var zz = x;
var a = x;
var A = x;
var aa = x;
var λ = x; // u03bb
var _ = x;
var $$ = x;
var $ = x;

var stringKeys = Object.getOwnPropertyNames(globalThis);

function assert_sameValue (x,y,m) {
    console.log (x == y);
}

function assert (x,m) {
    console.log (x);
}

assert_sameValue(stringKeys.length, 16);
assert_sameValue(stringKeys[0], '$', 'stringKeys[0] === "$"');
assert_sameValue(stringKeys[1], '$$', 'stringKeys[1] === "$$"');
assert_sameValue(stringKeys[2], 'A', 'stringKeys[2] === "A"');
assert_sameValue(stringKeys[3], 'Z', 'stringKeys[3] === "Z"');
assert_sameValue(stringKeys[4], '_', 'stringKeys[4] === "_"');
assert_sameValue(stringKeys[5], '__', 'stringKeys[5] === "__"');
assert_sameValue(stringKeys[6], 'a', 'stringKeys[6] === "a"');
assert_sameValue(stringKeys[7], 'aa', 'stringKeys[7] === "aa"');
assert_sameValue(stringKeys[8], 'az', 'stringKeys[8] === "az"');
assert_sameValue(stringKeys[9], 'default', 'stringKeys[9] === "default"');
assert_sameValue(stringKeys[10], 'z', 'stringKeys[10] === "z"');
assert_sameValue(stringKeys[11], 'za', 'stringKeys[11] === "za"');
assert_sameValue(stringKeys[12], 'zz', 'stringKeys[12] === "zz"');
assert_sameValue(stringKeys[13], '\u03bb', 'stringKeys[13] === "\u03bb"');
assert_sameValue(stringKeys[14], '\u03bc', 'stringKeys[14] === "\u03bc"');
assert_sameValue(stringKeys[15], '\u03c0', 'stringKeys[15] === "\u03c0"');

var allKeys = Reflect.ownKeys(globalThis);
assert(
  allKeys.length >= 17,
  'at least as many keys as defined by the module and the specification'
);
assert_sameValue(allKeys[0], '$', 'allKeys[0] === "$"');
assert_sameValue(allKeys[1], '$$', 'allKeys[1] === "$$"');
assert_sameValue(allKeys[2], 'A', 'allKeys[2] === "A"');
assert_sameValue(allKeys[3], 'Z', 'allKeys[3] === "Z"');
assert_sameValue(allKeys[4], '_', 'allKeys[4] === "_"');
assert_sameValue(allKeys[5], '__', 'allKeys[5] === "__"');
assert_sameValue(allKeys[6], 'a', 'allKeys[6] === "a"');
assert_sameValue(allKeys[7], 'aa', 'allKeys[7] === "aa"');
assert_sameValue(allKeys[8], 'az', 'allKeys[8] === "az"');
assert_sameValue(allKeys[9], 'default', 'allKeys[9] === "default"');
assert_sameValue(allKeys[10], 'z', 'allKeys[10] === "z"');
assert_sameValue(allKeys[11], 'za', 'allKeys[11] === "za"');
assert_sameValue(allKeys[12], 'zz', 'allKeys[12] === "zz"');
assert_sameValue(allKeys[13], '\u03bb', 'allKeys[13] === "\u03bb"');
assert_sameValue(allKeys[14], '\u03bc', 'allKeys[14] === "\u03bc"');
assert_sameValue(allKeys[15], '\u03c0', 'allKeys[15] === "\u03c0"');
assert(
  allKeys.indexOf(Symbol.toStringTag) > 15,
  'keys array includes Symbol.toStringTag'
);
