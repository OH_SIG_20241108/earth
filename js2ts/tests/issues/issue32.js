// Member function case
class Square{
    getarea() {
	    this.area = this.length * this.length;
	    return this.area
    }
    setlength(value) {
        this.length = value;
    }
}

let s = new Square(); 
s.setlength(6);
console.log(s.getarea());

// Get/set case
class Round{
    constructor (length) {
	    this.length = length;
    }
    get area() {
        let pi = 3.1415926;
	    return 2 * pi * (this.length / 2);
    }
    set area(value) {
        this.length = value;
    }
}

let r = new Round(123); 
r.area = 6;
console.log(r.area);
