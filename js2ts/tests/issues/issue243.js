// nested functions require unique naming

var a = true;
function bar() {
    return a;
}
console.log(bar());     // should infer result type of bar as boolean

function foo1() {
    var a = 111;
    console.log(a);
    function bar() {
        return a;
    }
    a = "abc";
    return bar();       // should infer result type of bar for foo1 (number | string)
}
console.log(foo1());

function foo2() {
    var a = 222;
    console.log(a);
    function bar() {
        return a;
    }
    a = 333;
    return bar();       // should infer result type of bar for foo2 (number)
}
console.log(foo2());

function foo3() {
    var a = "abc";
    console.log(a);
    if (a == "abc") {
        var a = "def";
        console.log(a);
    }
    console.log(a);
    function bar() {
        return a;
    }
    return bar();       // should infer result type of bar for foo3 (string)
}
console.log(foo3());
