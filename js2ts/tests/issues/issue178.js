crossOriginIsolated = true;

if (crossOriginIsolated) {
  const buffer = new SharedArrayBuffer(16);
  console.log (buffer);
} else {
  const buffer = new ArrayBuffer(16);
  console.log (buffer);
}
