let myError = EvalError("This is an eval error");
try {
  throw myError;
}
catch(e){
  console.log(e instanceof EvalError); // true
}
let errname = myError.name;
console.log(errname);

let errmsg = myError.message;
console.log(errmsg);

let errstack = myError.stack;
console.log(errstack);
