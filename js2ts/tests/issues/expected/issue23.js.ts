type JSDynamicObject = any;
"use strict";
const human : JSDynamicObject = {
    name : "John",
    age : 3
};
const handler : JSDynamicObject = {
    get : function (target : any, prop : any) : any {
        return prop in target ? target [prop] : `${prop} does not exist`;
    }
};
const user : JSDynamicObject = new Proxy (human, handler);
console.log (user.name);
console.log (user.age);
console.log (user.gender);
let ageValidator : JSDynamicObject = {
    set : function (obj : any, prop : string, value : any) : boolean {
        if (prop === 'age') {
            if (! Number.isInteger (value)) {
                throw new TypeError ('The age is not an integer');
            }
            if (value > 200) {
                throw new RangeError ('Invalid age');
            }
        }
        obj [prop] = value;
        return true;
    }
};
const person : JSDynamicObject = new Proxy ({}, ageValidator);
person.age = 30;
console.log (person.age);
