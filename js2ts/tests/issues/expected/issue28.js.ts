function f ( ... JSarguments : number []) : number {
    return JSarguments.length;
}
;
const len : number = f (1, 2, 3);
console.log (len);
