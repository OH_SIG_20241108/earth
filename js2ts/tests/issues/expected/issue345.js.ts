function f () : void {
    y = "abc";
    function g () : void {
        x = 1;
        z = true;
    }
    var x ! : number;
    var y ! : string;
}
var z ! : boolean;
