const timeoutId : number = globalThis.setTimeout (() : any => {
        console.log ("This message should not be displayed.");
    }, 1000);
globalThis.clearTimeout (timeoutId);
console.log ("setTimeout and clearTimeout test passed if no message is displayed after 1 second.");
let intervalCounter : number = 0;
const intervalId : number = globalThis.setInterval (() : any => {
        intervalCounter ++;
        console.log ("This message should be displayed 3 times.");
        if (intervalCounter === 3) {
            globalThis.clearInterval (intervalId);
            console.log ("setInterval and clearInterval test passed if this message is displayed after the 3rd time.");
        }
    }, 500);
