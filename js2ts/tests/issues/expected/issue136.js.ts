function rgb2lrgb (x : number) : number {
    return (x /= 255) <= 0.04045 ? x / 12.92 : Math.pow ((x + 0.055) / 1.055, 2.4);
}
