var t : number [] = [0];
var primes : number [] = t;
var pows : number [] = t;
var s_i : number [] = t;
var s_i2 : number [] = t;
var s_R : number [] = t;
var s_rm : number [] = t;
var s_q : number [] = t;
var s_n1 : number [] = t;
var s_a : number [] = t;
var s_r2 : number [] = t;
var s_n : number [] = t;
var s_b : number [] = t;
var s_d : number [] = t;
var s_x1 : number [] = t;
var s_x2 : number [] = t;
var s_aa : number [] = t;
var bpe : number = 0;
var mask : number = 0;
var radix : number = mask + 1;
function copy_ (x : number [], y : number []) : void {
    var i ! : number;
    var k : number = x.length < y.length ? x.length : y.length;
    for (i = 0; i < k; i ++) x [i] = y [i];
    for (i = k; i < x.length; i ++) x [i] = 0;
}
function copyInt_ (x : number [], n : number) : void {
    var i ! : number;
    var c ! : number;
    for (c = n, i = 0; i < x.length; i ++) {
        x [i] = c & mask;
        c >>= bpe;
    }
}
function dup (x : number []) : any [] {
    var i ! : any;
    var buff : any [] = [];
    copy_ (buff, x);
    return buff;
}
function findPrimes (n : number) : number [] {
    var i ! : number;
    var s ! : number [];
    var p ! : number;
    var ans ! : number [];
    s = [];
    for (i = 0; i < n; i ++) s [i] = 0;
    s [0] = 2;
    p = 0;
    for (; s [p] < n;) {
        for (i = s [p] * s [p]; i < n; i += s [p]) s [i] = 1;
        p ++;
        s [p] = s [p - 1] + 1;
        for (; s [p] < n && s [s [p]]; s [p] ++);
    }
    ans = [];
    for (i = 0; i < p; i ++) ans [i] = s [i];
    return ans;
}
function trueRandom () : number {
    return Math.random ();
}
function carry_ (x : number []) : void {
    var i ! : number;
    var k ! : number;
    var c ! : number;
    var b ! : number;
    k = x.length;
    c = 0;
    for (i = 0; i < k; i ++) {
        c += x [i];
        b = 0;
        if (c < 0) {
            b = - (c >> bpe);
            c += b * radix;
        }
        x [i] = c & mask;
        c = (c >> bpe) - b;
    }
}
function divide_ (x : number [], y : number [], q : number [], r : number []) : void {
}
function bitSize (s : number []) : void {
}
function randBigInt_ (sR : number [], z : number, zero : number) : void {
}
function greater (si : number [], sR : number []) : boolean {
    return false;
}
function addInt_ (a : number [], b : number) : void {
}
function add_ (a : number [], b : number []) : void {
}
function multInt_ (a : number [], b : number) : void {
}
function mult_ (a : number [], b : number []) : void {
}
function modInt (a : number [], b : number) : number {
    return 0;
}
function equalsInt (a : number [], b : number) : boolean {
    return true;
}
function millerRabinInt (a : number [], b : number) : boolean {
    return true;
}
function powMod_ (a : number [], b : number [], c : number []) : void {
}
function isZero (a : number []) : boolean {
    return true;
}
function GCD_ (a : number [], b : number []) : void {
}
function randTruePrime_ (ans : number [], k : number) : void {
    var c ! : number;
    var m ! : number;
    var pm ! : number;
    var dd ! : number;
    var j ! : number;
    var r ! : number;
    var B ! : number;
    var divisible ! : number;
    var w ! : number;
    var z ! : any;
    var zz ! : number;
    var recSize ! : number;
    if (primes.length == 0) primes = findPrimes (30000);
    if (pows.length == 0) {
        pows = [];
        for (j = 0; j < 512; j ++) {
            pows [j] = Math.pow (2, j / 511.- 1.);
        }
    }
    c = 0.1;
    m = 20;
    var recLimit : number = 20;
    if (s_i2.length != ans.length) {
        s_i2 = dup (ans);
        s_R = dup (ans);
        s_n1 = dup (ans);
        s_r2 = dup (ans);
        s_d = dup (ans);
        s_x1 = dup (ans);
        s_x2 = dup (ans);
        s_b = dup (ans);
        s_n = dup (ans);
        s_i = dup (ans);
        s_rm = dup (ans);
        s_q = dup (ans);
        s_a = dup (ans);
        s_aa = dup (ans);
    }
    if (k <= recLimit) {
        pm = (1 << (k + 2 >> 1)) - 1;
        copyInt_ (ans, 0);
        for (dd = 1; dd;) {
            dd = 0;
            ans [0] = 1 | 1 << k - 1 | Math.floor (trueRandom () * (1 << k));
            for (j = 1; j < primes.length && (primes [j] & pm) == primes [j]; j ++) {
                if (0 == ans [0] % primes [j]) {
                    dd = 1;
                    break;
                }
            }
        }
        carry_ (ans);
        return;
    }
    B = c * k * k;
    if (k > 2 * m) for (r = 1; k - k * r <= m;) r = pows [Math.floor (trueRandom () * 512)];
    else r = .5;
    recSize = Math.floor (r * k) + 1;
    randTruePrime_ (s_q, recSize);
    copyInt_ (s_i2, 0);
    s_i2 [Math.floor ((k - 2) / bpe)] |= 1 << (k - 2) % bpe;
    divide_ (s_i2, s_q, s_i, s_rm);
    z = bitSize (s_i);
    for (;;) {
        for (;;) {
            randBigInt_ (s_R, z, 0);
            if (greater (s_i, s_R)) break;
        }
        addInt_ (s_R, 1);
        add_ (s_R, s_i);
        copy_ (s_n, s_q);
        mult_ (s_n, s_R);
        multInt_ (s_n, 2);
        addInt_ (s_n, 1);
        copy_ (s_r2, s_R);
        multInt_ (s_r2, 2);
        for (divisible = 0, j = 0; j < primes.length && primes [j] < B; j ++) if (modInt (s_n, primes [j]) == 0 && ! equalsInt (s_n, primes [j])) {
            divisible = 1;
            break;
        }
        if (! divisible) if (! millerRabinInt (s_n, 2)) divisible = 1;
        if (! divisible) {
            addInt_ (s_n, - 3);
            for (j = s_n.length - 1; s_n [j] == 0 && j > 0; j --);
            for (zz = 0, w = s_n [j]; w; w >>= 1, zz ++);
            zz += bpe * j;
            for (;;) {
                randBigInt_ (s_a, zz, 0);
                if (greater (s_n, s_a)) break;
            }
            addInt_ (s_n, 3);
            addInt_ (s_a, 2);
            copy_ (s_b, s_a);
            copy_ (s_n1, s_n);
            addInt_ (s_n1, - 1);
            powMod_ (s_b, s_n1, s_n);
            addInt_ (s_b, - 1);
            if (isZero (s_b)) {
                copy_ (s_b, s_a);
                powMod_ (s_b, s_r2, s_n);
                addInt_ (s_b, - 1);
                copy_ (s_aa, s_n);
                copy_ (s_d, s_b);
                GCD_ (s_d, s_n);
                if (equalsInt (s_d, 1)) {
                    copy_ (ans, s_aa);
                    return;
                }
            }
        }
    }
}
console.log ('end');
