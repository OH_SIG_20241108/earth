type JSDynamicObject = any;
function Test (this : JSDynamicObject, exports_1 : any, context_1 : any) : JSDynamicObject {
    var b2_math_js_1 ! : any;
    var b2ReferenceFace ! : any;
    var b2EPColliderVertexType ! : JSDynamicObject;
    var b2EPCollider ! : any;
    return {
        execute : function (this : JSDynamicObject) : void {
            b2ReferenceFace = class b2ReferenceFace {
                i1 : number;
                i2 : number;
                sideOffset1 : number;
                sideOffset2 : number;
                constructor () {
                    this.i1 = 0;
                    this.i2 = 0;
                    this.sideOffset1 = 0;
                    this.sideOffset2 = 0;
                }
            }
            ;
            (function (b2EPColliderVertexType : string) : void {
                b2EPColliderVertexType [b2EPColliderVertexType ["e_isolated"] = 0] = "e_isolated";
                b2EPColliderVertexType [b2EPColliderVertexType ["e_concave"] = 1] = "e_concave";
                b2EPColliderVertexType [b2EPColliderVertexType ["e_convex"] = 2] = "e_convex";
            } (b2EPColliderVertexType || (b2EPColliderVertexType = {})));
            b2EPCollider.s_rf = new b2ReferenceFace ();
        }
    };
}
