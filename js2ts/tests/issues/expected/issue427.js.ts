const buffer : ArrayBuffer = new ArrayBuffer (8, {
    maxByteLength : 16
});
const float32 : Float32Array = new Float32Array (buffer);
const i : number = float32.byteLength;
console.log (i);
const j : number = float32.length;
console.log (j);
const uint8array1 : Uint8Array = new Uint8Array (buffer);
const k : number = uint8array1.byteOffset;
const uint8array2 : Uint8Array = new Uint8Array (buffer, 3);
const l : number = uint8array2.byteOffset;
const m : number = Float64Array.BYTES_PER_ELEMENT;
const buffer2 : ArrayBuffer = new ArrayBuffer (8);
const uint16 : Uint16Array = new Uint16Array (buffer2);
const u : ArrayBuffer = uint16.buffer;
console.log (uint16.buffer.byteLength);
