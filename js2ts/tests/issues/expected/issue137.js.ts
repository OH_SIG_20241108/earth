function rgb2lrgb (x : number) : number {
    return (x /= 255) <= 0.04045 ? x / 12.92 : (x + 0.055) / 1.055;
}
console.log (rgb2lrgb (68));
