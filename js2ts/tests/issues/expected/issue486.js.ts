function isSyntaxCharacter (c : string) : boolean {
    switch (c) {
        case "^" :
        case "$" :
        case "\\" :
        case "." :
        case "*" :
        case "+" :
        case "?" :
        case "(" :
        case ")" :
        case "[" :
        case "]" :
        case "{" :
        case "}" :
        case "|" :
            return true;
        default :
            return false;
    }
}
function isAlphaDigit (c : string) : boolean {
    return "0" <= c && c <= "9" || "A" <= c && c <= "Z" || "a" <= c && c <= "z";
}
for (var cu : number = 0x00; cu <= 0x7f; ++ cu) {
    var s : string = String.fromCharCode (cu);
    if (! isAlphaDigit (s) && ! isSyntaxCharacter (s) && s !== "/") {
        assert.throws (SyntaxError, function () : void {
            RegExp ("\\" + s, "u");
        }, "Invalid IdentityEscape in AtomEscape: '\\" + s + "'");
    }
}
for (var cu : number = 0x00; cu <= 0x7f; ++ cu) {
    var s : string = String.fromCharCode (cu);
    if (! isAlphaDigit (s) && ! isSyntaxCharacter (s) && s !== "/" && s !== "-") {
        assert.throws (SyntaxError, function () : void {
            RegExp ("[\\" + s + "]", "u");
        }, "Invalid IdentityEscape in ClassEscape: '\\" + s + "'");
    }
}
