type JSDynamicObject = any;
var x : null | number = null ?? 123;
console.log (x);
var y : undefined | string = undefined ?? "abc";
console.log (y);
var z ! : JSDynamicObject | number;
var obj : JSDynamicObject = {
    toString () : any {
        return null;
    },
    valueOf () : any {
        return null;
    }
};
z = obj ?? 1;
console.log (z);
var w : number = 42 ?? 1;
console.log (w);
var u : number | undefined = 42 ?? undefined;
console.log (u);
