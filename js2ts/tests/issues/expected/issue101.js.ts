let set_string : Set <any> = new Set <any> (['red', 'green', 'blue']);
let set_number : Set <any> = new Set <any> ([1, 2, 3]);
let set_object : Set <any> = new Set <any> ([{
    a : 1,
    b : 2
}, {
    s1 : '1',
    s2 : '2'
}]);
let set_mix : Set <any> = new Set <any> ([undefined, 'hello', 100, null, {
    a : 1
}]);
