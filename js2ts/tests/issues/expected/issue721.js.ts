type JSDynamicObject = any;
function graticule (this : JSDynamicObject) : any {
    var X1 ! : any;
    var X0 ! : any;
    var Y1 ! : any;
    var Y0 ! : any;
    this.extentMajor = function ( ... JSarguments : any []) : any {
        let _ : any = JSarguments [0];
        if (! JSarguments.length) return [[X0, Y0], [X1, Y1]];
        X0 = (+ _ [0] [0]);
        X1 = (+ _ [1] [0]);
        Y0 = (+ _ [0] [1]);
        Y1 = (+ _ [1] [1]);
        if (X0 > X1) _ = X0, X0 = X1, X1 = _;
        if (Y0 > Y1) _ = Y0, Y0 = Y1, Y1 = _;
        return _;
    }
}
