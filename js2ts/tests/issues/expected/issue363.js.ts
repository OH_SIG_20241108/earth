let myError : Error = EvalError ("This is an eval error");
try {
    throw myError;
}
catch (e : any) {
    console.log (e instanceof EvalError);
}
let errname : string = myError.name;
console.log (errname);
let errmsg : string = myError.message;
console.log (errmsg);
let errstack : any = myError.stack;
console.log (errstack);
