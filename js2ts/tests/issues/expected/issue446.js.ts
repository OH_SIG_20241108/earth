function example (arg1 : any, arg2 ? : any) : void {
    if (typeof arg1 === 'function') {
        console.log ('arg1 is a function');
    } else {
        console.log ('arg1 is not a function');
    }
}
example ('string');
example (function () : void {
});
