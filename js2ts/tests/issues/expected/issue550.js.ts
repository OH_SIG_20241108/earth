type JSDynamicObject = any;
function Shape (this : JSDynamicObject) : void {
    this.x = 0;
    this.y = 0;
}
function Circle (this : JSDynamicObject) : void {
    Shape.call (this);
}
console.log (new Circle ().x);
