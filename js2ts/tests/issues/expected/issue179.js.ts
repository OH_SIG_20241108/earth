const littleEndian : any = (() : boolean => {
        const buffer : ArrayBuffer = new ArrayBuffer (2);
        var dataview : DataView = new DataView (buffer);
        dataview.setInt16 (0, 256, true);
        var ret : boolean = new Int16Array (buffer) [0] === 256;
        return ret;
    }) ();
console.log (littleEndian);
