type JSNoValue = any;
function foo (a : number) : string [] | Error | JSNoValue {
    if (a === 1) {
        return;
    }
    if (a === 2) {
        return new Error ('error');
    }
    if (a === 3) {
        return [String (a), String (a)];
    }
}
var b : string [] | Error = foo (1);
var c : string [] | Error = foo (3);
console.log (b, c);
