class C {
    p0 : number;
    p1 : number = 42;
    p2 : string = "abc";
    p3 : boolean = true;
    constructor () {
        this.p0 = 42;
    }
    f1 () : number {
        return 42;
    }
    f2 (x : number) : number {
        return x;
    }
    f3 () : number {
        return this.p1;
    }
    f4 (x : string) : string {
        return x;
    }
}
var o1 : C = new C ();
var v0 : number = o1.p0;
console.log (v0);
var v1 : number = o1.p1;
console.log (v1);
var v2 : string = o1.p2;
console.log (v2);
if (o1.p3) {
    console.log (o1.p3);
}
var v3 : number = o1.f1 ();
console.log (v3);
var v4 : number = o1.f2 (v1);
console.log (v4);
var v5 : number = o1.f3 ();
console.log (v5);
var v6 : string = o1.f4 (v2);
console.log (v6);
