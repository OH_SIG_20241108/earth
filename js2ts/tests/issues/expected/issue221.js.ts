var x : string | number = "abc";
x = (x as any) + 1;
console.log (x);
x = 41;
x = (x as any) + 1;
console.log (x);
function f (x : number | string) : void {
    x = (x as any) + 1;
    console.log (x);
}
f ("abc");
f (41);
