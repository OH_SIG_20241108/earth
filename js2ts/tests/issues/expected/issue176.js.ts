type JSDynamicObject = any;
var reHex : RegExp = /^#([0-9a-f]{3,8})$/;
var reI : string = "\\s*([+-]?\\d+)\\s*";
var reN : string = "\\s*([+-]?(?:\\d*\\.)?\\d+(?:[eE][+-]?\\d+)?)\\s*";
var reP : string = "\\s*([+-]?(?:\\d*\\.)?\\d+(?:[eE][+-]?\\d+)?)%\\s*";
var reHex : RegExp = /^#([0-9a-f]{3,8})$/;
var reRgbaInteger : RegExp = new RegExp ("^rgba\\(".concat (reI, ",").concat (reI, ",").concat (reI, ",").concat (reN, "\\)$"));
var reHslPercent : RegExp = new RegExp ("^hsl\\(".concat (reN, ",").concat (reP, ",").concat (reP, "\\)$"));
function color (format : string) : any {
    var m ! : string [] | null | number;
    format = (format + "").trim ().toLowerCase ();
    var l ! : number;
    return (m = reHex.exec (format)) ? (l = m [1].length, m = parseInt (m [1], 16), l === 6 ? (m = reRgbaInteger.exec (format)) ? rgba (m [1], m [2], m [3], m [4]) : null : null) : null;
}
function rgba (r : number | string, g : number | string, b : number | string, a : string) : any {
    if ((+ a) <= 0) r = g = b = NaN;
    return new Rgb (r, g, b, a);
}
var Rgb : any = (function (this : JSDynamicObject) : any {
    function Rgb (this : JSDynamicObject, r : any, g : any, b : any, opacity : any) : void {
        this.r = (+ r);
        this.g = (+ g);
        this.b = (+ b);
        this.opacity = (+ opacity);
    }
    return Rgb;
}
());
console.log (color ("#abcedf"));
