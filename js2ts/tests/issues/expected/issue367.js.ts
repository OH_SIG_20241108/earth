const collator : Intl.Collator = new Intl.Collator ("en");
const collatorResult : any = ["apple", "banana", "Cherry"].sort (collator.compare);
console.log ("Collator:", collatorResult);
const dateTimeFormat : Intl.DateTimeFormat = new Intl.DateTimeFormat ("en-US", {
    year : "numeric",
    month : "long",
    day : "numeric",
});
const formattedDate : string = dateTimeFormat.format (new Date ());
console.log ("DateTimeFormat:", formattedDate);
const listFormat : Intl.ListFormat = new Intl.ListFormat ("en-US", {
    style : "long",
    type : "conjunction",
});
const formattedList : string = listFormat.format (["apple", "banana", "cherry"]);
console.log ("ListFormat:", formattedList);
const numberFormat : Intl.NumberFormat = new Intl.NumberFormat ("en-US", {
    style : "currency",
    currency : "USD",
});
const formattedNumber : string = numberFormat.format (123456.789);
console.log ("NumberFormat:", formattedNumber);
const pluralRules : Intl.PluralRules = new Intl.PluralRules ("en-US", {
    type : "ordinal"
});
const formattedPlural : string = pluralRules.select (22);
console.log ("PluralRules:", formattedPlural);
const relativeTimeFormat : Intl.RelativeTimeFormat = new Intl.RelativeTimeFormat ("en-US", {
    numeric : "auto",
});
const formattedRelativeTime : string = relativeTimeFormat.format (- 1, "day");
console.log ("RelativeTimeFormat:", formattedRelativeTime);
