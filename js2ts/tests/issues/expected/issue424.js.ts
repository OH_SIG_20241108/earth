type JSNoValue = any;
function searchVersion (dataString : string) : number | JSNoValue {
    let index : number = dataString.indexOf ('1234');
    if (index === - 1) return;
    return parseFloat (dataString.substring (index + 1));
}
console.log (searchVersion ('version1234'));
console.log (searchVersion ('version5678'));
