type JSDynamicObject = any;
var global01 : number = 1;
console.log (global01);
var global02 : JSDynamicObject = {};
console.log (global02);
global02.field = "x";
console.log (global02.field);
var global11 ! : number;
var global12 ! : number;
var global13 ! : JSDynamicObject;
var global14 ! : number;
{
    global11 = 0;
    console.log (global11);
    let local11 : number = 1;
    console.log (local11);
    global12 = local11;
    console.log (global12);
    const local12 : number = global01;
    console.log (local12);
    global13 = {};
    console.log (global13);
    global13.field = "x";
    console.log (global13.field);
    if (true) {
        let innerlocal1 : number = 1;
        console.log (innerlocal1);
        global14 = 2;
        console.log (global14);
    }
}
var global2 ! : number;
var localized2 ! : number;
function f21 () : void {
    global2 = 1;
    console.log (global2);
    localized2 = 1;
    console.log (localized2);
}
function f22 () : void {
    global2 = 2;
    console.log (global2);
    let local2 : number = 1;
    console.log (local2);
}
f21 ();
f22 ();
function f31 () : void {
    var local3 : string = "abc";
    console.log (local3);
}
function f32 () : void {
    let local3 : number = 3;
    console.log (local3);
}
function f33 () : void {
    const local3 : boolean = true;
    console.log (local3);
}
function f34 () : void {
    var local3 ! : string;
    function f35 () : void {
        var local3 : number = 1;
        console.log (local3);
    }
    local3 = "abc";
    console.log (local3);
}
f31 ();
f32 ();
f33 ();
f34 ();
var global41 ! : string | number;
let global42 : number = 42;
console.log (global42);
const global43 : number = 43;
console.log (global43);
var global44 ! : number;
function f4 () : void {
    global41 = "abc";
    console.log (global41);
    global42 = 42;
    console.log (global42);
    global44 = global43 + 1;
    console.log (global44);
}
f4 ();
global41 = 41;
console.log (global41);
