export default function transpose (matrix : any [] []) : any [] [] {
    if (! (n = matrix.length)) return [];
    for (var i : number = - 1, m : number = 2, transpose : any [] [] = []; ++ i < m;) {
        for (var j : number = - 1, n : number, row : any [] = transpose [i] = []; ++ j < n;) {
            row [j] = matrix [j] [i];
        }
    }
    return transpose;
}
function length (d : any) : number {
    return d.length;
}
