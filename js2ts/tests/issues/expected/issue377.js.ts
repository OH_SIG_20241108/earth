var vals : bigint [] = [18446744073709551618n, 9223372036854775810n, 2n, 0n, - 2n, - 9223372036854775810n, - 18446744073709551618n,];
var typedArray : BigUint64Array = new BigUint64Array (vals);
function sameValue (a : bigint, b : bigint, c : string) : void {
}
sameValue (typedArray [0], 2n, "ToBigUint64(2n ** 64n + 2n) => 2n");
sameValue (typedArray [1], 9223372036854775810n, "ToBigUint64(2n ** 63n + 2n) => 9223372036854775810");
sameValue (typedArray [2], 2n, "ToBigUint64(2n) => 2n");
sameValue (typedArray [3], 0n, "ToBigUint64(0n) => 0n");
sameValue (typedArray [4], 18446744073709551614n, "ToBigUint64( -2n) => 18446744073709551614n");
sameValue (typedArray [5], 9223372036854775806n, "ToBigUint64( -(2n ** 63n) - 2n) => 9223372036854775806n");
sameValue (typedArray [6], 18446744073709551614n, "ToBigUint64( -(2n ** 64n) - 2n) => 18446744073709551614n");
