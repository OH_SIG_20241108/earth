type JSDynamicObject = any;
function dispatch ( ... JSarguments : any []) : JSDynamicObject | any {
    for (var i : number = 0, n : number = JSarguments.length, _ : JSDynamicObject = {}, t : string; i < n; ++ i) {
        if (! (t = JSarguments [i] + "") || t in _ || /[\s.]/.test (t)) throw new Error ("illegal type: " + t);
        _ [t] = [];
    }
    return new Dispatch (_);
}
function Dispatch (this : JSDynamicObject, _ : JSDynamicObject) : JSDynamicObject {
    this._ = _;
}
Dispatch.prototype = dispatch.prototype = {
    constructor : Dispatch,
    on : function (this : JSDynamicObject, typename : any, callback : any) : any {
        return this;
    },
    call : function (this : JSDynamicObject, ... JSarguments : any []) : void {
        let type : any = JSarguments [0];
        let that : any = JSarguments [1];
        if ((n = JSarguments.length - 2) > 0) for (var args : any [] = [], i : number = 0, n : number, t : any []; i < n; ++ i) args [i] = JSarguments [i + 2];
        if (! this._.hasOwnProperty (type)) throw new Error ("unknown type: " + type);
        for (t = this._ [type], i = 0, n = t.length; i < n; ++ i) t [i].value.apply (that, args);
    },
    apply : function (this : JSDynamicObject, type : any, that : any, args : any) : void {
        if (! this._.hasOwnProperty (type)) throw new Error ("unknown type: " + type);
        for (var t : any [] = this._ [type], i : number = 0, n : number = t.length; i < n; ++ i) t [i].value.apply (that, args);
    }
};
export default dispatch;
