type JSDynamicObject = any;
function cornerTangents (x0 : any, y0 : any, x1 : number, y1 : number, r1 : number, rc : number, cw : number) : JSDynamicObject {
    return {
        cx : 1,
        cy : 1,
        x01 : - 1,
        y01 : - 1,
        x11 : 0.9,
        y11 : 0.8
    };
}
var r1 : number = 9;
var rc1 : number = 2;
var cw : number = 9;
var x01 : number = 0.9;
var y01 : number = 0.8;
var x10 : number = 4;
var y10 : number = 9;
var t0 ! : JSDynamicObject;
var t1 ! : JSDynamicObject;
if (9 < x01) {
    console.log ("case 1");
    var x00 : number = 7;
    var y00 : number = 9;
    var x11 : number = 8;
    var y11 : number = 6;
} else {
    console.log ("case 2");
    console.log ("x00 is " + x00);
    console.log ("x11 is " + x11);
    console.log ("y00 is " + y00);
    console.log ("y11 is " + y11);
    t0 = cornerTangents (x00, y00, x01, y01, r1, rc1, cw);
    t1 = cornerTangents (x11, y11, x10, y10, r1, rc1, cw);
}
console.log (t0);
console.log (t1);
