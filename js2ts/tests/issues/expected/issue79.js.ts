function rgbn (n : number) : Rgb {
    return new Rgb (n >> 16 & 0xff, n >> 8 & 0xff, n & 0xff, 1);
}
function rgba (r : number, g : number, b : number, a : any) : Rgb {
    if (a <= 0) r = g = b = NaN;
    return new Rgb (r, g, b, a);
}
function rgbConvert (o : any) : Rgb {
    o = o.rgb ();
    return new Rgb (o.r, o.g, o.b, o.opacity);
}
function rgb ( ... JSarguments : any []) : Rgb {
    let r : any = JSarguments [0];
    let g : any = JSarguments [1];
    let b : any = JSarguments [2];
    let opacity : any = JSarguments [3];
    return JSarguments.length === 1 ? rgbConvert (r) : new Rgb (r, g, b, opacity == null ? 1 : opacity);
}
class Rgb {
    r : number;
    g : number;
    b : number;
    opacity : number;
    constructor (r : any, g : any, b : any, opacity : any) {
        this.r = (+ r);
        this.g = (+ g);
        this.b = (+ b);
        this.opacity = (+ opacity);
    }
}
console.log (rgbn (125));
