const int16 : Int16Array = new Int16Array (2);
int16 [0] = 42;
console.log (int16 [0]);
console.log (int16.length);
console.log (int16.BYTES_PER_ELEMENT);
const x : Int16Array = new Int16Array ([21, 31]);
console.log (x [1]);
const y : Int16Array = new Int16Array (x);
console.log (y [0]);
const buffer : ArrayBuffer = new ArrayBuffer (16);
const z : Int16Array = new Int16Array (buffer, 2, 4);
console.log (z.byteOffset);
