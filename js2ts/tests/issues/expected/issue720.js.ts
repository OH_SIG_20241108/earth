function A () : void {
}
A.prototype = {
    a : 1
};
function B () : any {
    var b : any = new A ();
    return b.a;
}
console.log (B ());
