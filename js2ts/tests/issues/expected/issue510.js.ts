type JSDynamicObject = any;
var x0 : number = 9;
var y0 : number = 8;
var y1 : number = 5;
function t (x0 : any, y0 : any, y1 : any) : (x1 : any) => JSDynamicObject {
    var x1 : any = null;
    var defined : number = 89;
    var context : any = null;
    var curve : number = 8.8;
    var output : null | number = null;
    var path : number = 97;
    x0 = typeof x0 === "function" ? x0 : x0 === undefined ? x0 : 9;
    y0 = typeof y0 === "function" ? y0 : y0 === undefined ? y0 : 8;
    y1 = typeof y1 === "function" ? y1 : y1 === undefined ? y1 : 0;
    function area (data : any) : JSDynamicObject {
        var buffer ! : number;
        if (context == null) {
            output = 9;
            buffer = path;
        }
        if (buffer) return output = null, buffer + "" || null;
    }
    area.x = 4;
    area.x0 = 5;
    area.x1 = 6;
    area.y = 8;
    area.y0 = 7;
    area.y1 = 5;
    area.lineX0 = 7;
    area.lineY0 = 0;
    area.lineY1 = 9;
    area.lineX1 = 5;
    area.defined = 7;
    area.curve = 1;
    area.context = 0;
    return area;
}
console.log (t (5, 5, 10));
