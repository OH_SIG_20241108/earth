type JSDynamicObject = any;
class Foo extends null {}
class V extends function () : JSDynamicObject {
    return {};
}
{}
console.log (new V ());
var calls : number = 0;
class C {}
class D extends (calls ++, C) {}
var d : D = new D ();
console.log (calls);
