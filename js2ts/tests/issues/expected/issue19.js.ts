function sum ( ... args : number []) : any {
    return args.reduce ((previous : any, current : any) : any => {
            return previous + current;
        });
}
console.log (sum (1, 2, 3));
console.log (sum (1, 2, 3, 4));
console.log (sum (1, 2, 3, 4, 5));
