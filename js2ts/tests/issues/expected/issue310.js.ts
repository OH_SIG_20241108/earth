var empty : any = Object.create (null);
var C ! : any;
var value ! : any;
for (C = class {
    static get ['x' in empty] () : any {
        return 'via get';
    }
}
;;) {
    value = C.false;
    break;
}
