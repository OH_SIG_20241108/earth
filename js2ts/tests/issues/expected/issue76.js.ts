async function f () : Promise <number> {
    return 1;
};
async function g () : Promise <string> {
    return "abc";
};
async function h (x : number, y : number, z : number) : Promise <number> {
    return x + y + z;
};
h (1, 2, 3);
var j : any = (async (x : any, y : any) : Promise <any> => {
        return x + y;
    }) (40, 2);
console.log (j);
