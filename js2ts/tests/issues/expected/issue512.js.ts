const abs : (x1 : number) => number = Math.abs;
const atan2 : (x1 : number, x2 : number) => number = Math.atan2;
const cos : (x1 : number) => number = Math.cos;
const max : ( ... x1 : number []) => number = Math.max;
const min : ( ... x1 : number []) => number = Math.min;
const sin : (x1 : number) => number = Math.sin;
const sqrt : (x1 : number) => number = Math.sqrt;
console.log (abs (- 42));
