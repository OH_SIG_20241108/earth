class a {
    foo ( ... args : number []) : number {
        if (args [2] == 1) {
            return this._foo (args [0], args [1]);
        } else {
            return this._foo (args [0], args [1], args [2]);
        }
    }
    _foo (a : number, b : number, c ? : number) : number {
        if (c) {
            return a + b + c;
        }
        return a + b;
    }
}
var b : a = new a ();
console.log (b.foo (1, 2, 3));
