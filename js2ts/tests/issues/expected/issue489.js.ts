type JSDynamicObject = any;
function foo (this : JSDynamicObject, bucket : any, keys : any) : string | any {
    var self : any = this;
    if (Array.isArray (keys)) {
        return keys.map (function (key : any) : string {
            return self.prefix + '_' + bucket + '@' + key;
        });
    } else {
        return self.prefix + '_' + bucket + '@' + keys;
    }
}
var obj : JSDynamicObject = {
    prefix : 'myPrefix'
};
var result : any = foo.call (obj, 'myBucket', ['key1', 'key2']);
console.log (result);
