import {Basis} from "./issue537-2";
class Bundle {
    _basis : any;
    _beta : any;
    constructor (context : any, beta : any) {
        this._basis = new Basis (context);
        this._beta = beta;
    }
}
