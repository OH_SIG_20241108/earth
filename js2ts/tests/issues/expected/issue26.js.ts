type JSDynamicObject = any;
class User {
    firstName : string;
    lastName : string;
    constructor (firstName : string, lastName : string) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    get fullName () : string {
        return `${this.firstName} ${this.lastName}`;
    }
}
;
let john : JSDynamicObject = new User ('John', 'Resig');
if (Reflect.defineProperty (john, 'age', {
    writable : true,
    configurable : true,
    enumerable : false,
    value : 33,
})) {
    console.log (john.age);
} else {
    console.log ('Cannot define the age property on the user object.');
}
