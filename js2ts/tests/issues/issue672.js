function mutation (x,y) {}

function relativeTimeWithMutation(number, withoutSuffix, key) {
    const format = {
      mm: 'munutenn',
      MM: 'miz',
      dd: 'devezh'
    }
    return `${number} ${mutation(format[key], number)}`
  }
