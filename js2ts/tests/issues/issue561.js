function x(d) {
  return d.x + d.vx;
}
function y(d) {
  return d.y + d.vy;
}
function z(radius) {
    function apply(quad, x0, y0, x1, y1) {
      var data = quad.data,node;
      if (data) {
          var x = x0,
              y = y0,
              l = x * x + y * y;
          if (x0>1) {
            node.vx += (x *= l) ;
            node.vy += (y *= l);     
        }
      }
      return 1;
    }
  return apply;
}
console.log (z(42));
