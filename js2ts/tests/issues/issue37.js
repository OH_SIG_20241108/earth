var Circle = function (radius) {
  this.radius = radius;
  this.draw = function() {
    console.log('draw circle');
  }
}

const circle = new Circle(1);
circle.draw();

function Rect (length,width) {
  this.length = length;
  this.width = width;
  this.draw = function() {
    console.log('draw rect');
  }
}

const square = new Rect(4,4);
square.draw();
