// Boolean operands of arithmetic operations
left1 = 41;
right1 = new Boolean(true);
result1 = left1 + right1;
console.log(result1);

left2 = true;
right2 = new Number(41);
result2 = left2 + right2;
console.log(result2);

left3 = new Boolean(true);
right3 = new Number(41);
result3 = left3 + right3;
console.log(result3);

left4 = new String("4");
right4 = "2";
result4 = left4 + right4;
console.log(result4);

left5 = function () { return 21; };
right5 = function () { return 21; };
result5 = left5() + right5();
console.log(result5);

left6 = 40n;
right6 = 2n;
result6 = left6 + right6;
console.log(result6);

left7 = 1;
right7 = 41;
result7 = (left7 == 1) + (right7 < 42);
console.log (40 + result7);

left8 = true;
right8 = Boolean ("true");
result8 = left8 + right8;
console.log (40 + result8);

left9 = true;
right9 = 42;
result9 = left9 * right9;
console.log (result9);

left10 = 84;
right10 = new Boolean (true);
result10 = left10 / (right10 + right10);
console.log (result10);
