console.log (1n <= 0n);
console.log (123 <= 456);
console.log (1.123 <= 2.456);
console.log ("abc" <= "def");
let a = 123;
let b = "abc";
console.log (a <= b); // error TS2365: Operator '<=' cannot be applied to types 'number' and 'string'.
