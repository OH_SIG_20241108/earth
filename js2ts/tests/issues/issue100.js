class Square {
    constructor (length) {
        this.length = length;
    }
    get perimeter ()  {
        return  this.length +  this.length +  this.length +  this.length ;
    }
    set perimeter (value) {
        this.length = value;
    }
}
let p1  = new Square (123);
console.log (p1.perimeter)
let p2 = new Square ("123");
console.log (p2.perimeter)
