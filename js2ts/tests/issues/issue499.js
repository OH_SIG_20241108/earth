function toArray(value) {
    if (Array.isArray(value)) {
      return value
    } else if (Array.from) {
      return Array.from(value)
    } else {
      return [].slice.call(value)
    }
  }
console.log(toArray("hello"))
console.log(toArray([ 'h', 'e', 'l', 'l', 'o' ]))
