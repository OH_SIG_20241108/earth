//Octal literals
const num1 = 055;
console.log(num1); // 45

const invalidNum = 028;
console.log(invalidNum); // treated as decimal 28
