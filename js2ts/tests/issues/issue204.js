let a1 = false ? false : 123;
console.log(a1);

let b1 = true ? "abc" : true;
console.log(b1);

let c1 = true ? 2 : 5;
console.log(c1);
