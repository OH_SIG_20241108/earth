// Global Scope
global01 = 1;
console.log(global01);
global02 = {};
console.log(global02);
global02.field = "x";
console.log(global02.field);

// Block Scope
// Before ES6 (2015), JavaScript had only Global Scope and Function Scope.
// ES6 introduced two important new JavaScript keywords: let and const.
// These two keywords provide Block Scope in JavaScript.
// Variables declared inside a { } block cannot be accessed from outside the block:
{
    global11 = 0;
    console.log(global11);
    let local11 = 1;
    console.log(local11);
    global12 = local11;
    console.log(global12);
    const local12 = global01;
    console.log(local12);
    global13 = {};
    console.log(global13);
    global13.field = "x";
    console.log(global13.field);
    if (true) {
        let innerlocal1 = 1;
        console.log(innerlocal1);
        global14 = 2;
        console.log(global14);
    }
}

// Local Scope
// Variables declared within a JavaScript function, become LOCAL to the function.
// Local variables have Function Scope:
// They can only be accessed from within the function.
// Since local variables are only recognized inside their functions, variables with the same name can be used in different functions.
// Local variables are created when a function starts, and deleted when the function is completed.

function f21() {
    global2 = 1;
    console.log(global2);
    localized2 = 1;
    console.log(localized2);
}

function f22() {
    global2 = 2;
    console.log(global2);
    let local2 = 1;
    console.log(local2);
}

f21();
f22();

// Function Scope
// JavaScript has function scope: Each function creates a new scope.
// Variables defined inside a function are not accessible (visible) from outside the function.
// Variables declared with var, let and const are quite similar when declared inside a function.

function f31() {
    var local3;
    local3 = "abc";   
    console.log(local3);
}

function f32() {
    let local3 = 3;
    console.log(local3);
}

function f33() {
    const local3 = true;
    console.log(local3);
}

function f34() {
    var local3;
    function f35() {
        var local3;
        local3 = 1;
        console.log(local3);
    }
    local3 = "abc";
    console.log(local3);
}

f31();
f32();
f33();
f34();

// Global Scope
// Variables declared Globally (outside any function) have Global Scope.
// Global variables can be accessed from anywhere in a JavaScript program.
// Variables declared with var, let and const are quite similar when declared outside a block.
// They all have Global Scope:

var global41;
let global42 = 42;
console.log(global42);
const global43 = 43;
console.log(global43);

function f4() {
    global41 = "abc";
    console.log(global41);
    global42 = 42;
    console.log(global42);
    global44 = global43 + 1;
    console.log(global44);
}

f4();

global41 = 41;
console.log(global41);

