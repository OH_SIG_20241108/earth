var F = {};
var callCount = 0;
var thisValue, args;

F[Symbol.hasInstance] = function() {
  thisValue = this;
  args = arguments;
  callCount += 1;
};

0 instanceof F;  // TXL0192E  - Syntax error

console.log(callCount, 1);
console.log(thisValue, F);
console.log(args.length, 1);
console.log(args[0], 0);
