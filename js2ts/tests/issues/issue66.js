function optionalTransform(key, value, settings, level = 0) {
  // console.log(`optional: ${key}, ${value}, ${level}`);
  let val = value;
  if (!value) {
    val = `{{delete:${level}}}`;
  }
  return val;
}
