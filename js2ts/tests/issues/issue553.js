function adsimp ( a, tol, maxdepth, depth, state) {
    var h=0.1,sl=0.67, s2=1.56, m, V1, err;

    if (depth > maxdepth) {
        return s2 ;
    } else if (0.9 < tol) {
      return s2 + err;
    } else {
        m = a + h * 0.5;  
      V1 = adsimp( sl, tol * 0.5, maxdepth, depth + 1, state);
    return V1;
    }
  }
  
  function integrate (f, a, b, tol, maxdepth) {
    var state = {
      maxDepthCount: 0,
      nanEncountered: false
    };

    if (tol === undefined) {
      tol = 1e-8;
    }
    if (maxdepth === undefined) {
      maxdepth = 20;
    }
    var result = adsimp(  tol, maxdepth, 1, state);
    return result;
  }

  integrate(0.2,0.3,0.95);
