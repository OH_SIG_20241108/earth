export default function transpose(matrix) {
  if (!(n = matrix.length)) return [];
  for (var i = -1, m = 2, transpose = new Array(m); ++i < m;) {
    for (var j = -1, n, row = transpose[i] = new Array(n); ++j < n;) {
      row[j] = matrix[j][i];
    }
  }
  return transpose;
}

function length(d) {
  return d.length;
}
