function verifyProperty(obj, name, desc, options) {
   // assert(arguments.length > 2, 'verifyProperty should receive at least 3 arguments: obj, name, and descriptor');
  // assert  and $ERROR functions are commented  
    var originalDesc = Object.getOwnPropertyDescriptor(obj, name);
    var nameStr = String(name);
    if (desc === undefined) {
        //assert.sameValue(originalDesc, undefined, "obj['" + nameStr + "'] descriptor should be undefined");
        return true;
    }
    //assert(Object.prototype.hasOwnProperty.call(obj, name), "obj should have an own property " + nameStr);
    //assert.notSameValue(desc, null, "The desc argument should be an object or undefined, null");
    //assert.sameValue(typeof desc, "object", "The desc argument should be an object or undefined, " + String(desc));
    var failures = [];
    if (Object.prototype.hasOwnProperty.call(desc, 'value')) {
        if (desc.value !== originalDesc.value) {
            failures.push("descriptor value should be " + desc.value);
        }
    }
    if (Object.prototype.hasOwnProperty.call(desc, 'enumerable')) {
        if (desc.enumerable !== originalDesc.enumerable || desc.enumerable !== isEnumerable(obj, name)) {
            failures.push('descriptor should ' + (desc.enumerable ? '' : 'not ') + 'be enumerable');
        }
    }
    if (Object.prototype.hasOwnProperty.call(desc, 'writable')) {
        if (desc.writable !== originalDesc.writable || desc.writable !== isWritable(obj, name)) {
            failures.push('descriptor should ' + (desc.writable ? '' : 'not ') + 'be writable');
        }
    }
    if (Object.prototype.hasOwnProperty.call(desc, 'configurable')) {
        if (desc.configurable !== originalDesc.configurable || desc.configurable !== isConfigurable(obj, name)) {
            failures.push('descriptor should ' + (desc.configurable ? '' : 'not ') + 'be configurable');
        }
    }
    //assert(!failures.length, failures.join('; '));
    if (options && options.restore) {
        Object.defineProperty(obj, name, originalDesc);
    }
    return true;
}
function isEnumerable(obj, name) {
    var stringCheck = false;
    if (typeof name === "string") {
        for (var x in obj) {
            if (x === name) {
                stringCheck = true;
                break;
            }
        }
    }
    else {
        stringCheck = true;
    }
    return stringCheck && Object.prototype.hasOwnProperty.call(obj, name) && Object.prototype.propertyIsEnumerable.call(obj, name);
}
function isWritable(obj, name, verifyProp, value) {
    var newValue = value || "unlikelyValue";
    var hadValue = Object.prototype.hasOwnProperty.call(obj, name);
    var oldValue = obj[name];
    var writeSucceeded;
    try {
        obj[name] = newValue;
    }
    catch (e) {
        if (!(e instanceof TypeError)) {
           // $ERROR("Expected TypeError, got " + e);
        }
    }
    writeSucceeded = isEqualTo(obj, verifyProp || name, newValue);
    if (writeSucceeded) {
        if (hadValue) {
            obj[name] = oldValue;
        }
        else {
            delete obj[name];
        }
    }
    return writeSucceeded;
}
function isEqualTo(obj, name, expectedValue) {
    var actualValue = obj[name];
    //return assert._isSameValue(actualValue, expectedValue);
}
function isConfigurable(obj, name) {
    try {
        delete obj[name];
    }
    catch (e) {
        if (!(e instanceof TypeError)) {
            //$ERROR("Expected TypeError, got " + e);
        }
    }
    return !Object.prototype.hasOwnProperty.call(obj, name);
}

o = {a:1,b:2}
console.log (verifyProperty (o, "o"))
