"use strict";
const human = { name: "John", age: 3 };
const handler = ({ get: function (target, prop) {
        return prop in target ? target[prop] : `${prop} does not exist`;
    }
});
const user = new Proxy(human, handler);
console.log(user.name);
console.log(user.age);
console.log(user.gender);
let ageValidator = ({ set: function (obj, prop, value) {
        if (prop === 'age') {
            if (!Number.isInteger(value)) {
                throw new TypeError('The age is not an integer');
            }
            if (value > 200) {
                throw new RangeError('Invalid age');
            }
        }
        obj[prop] = value;
        return true;
    }
});
const person = new Proxy({}, ageValidator);
person.age = 30;
console.log(person.age);
