const bigInt = 9007199254740991n;
const bigIntConstructorRep = BigInt (9007199254740991);
const bigIntStringRep = BigInt ("9007199254740991");
const previousMaxNum = BigInt (Number.MAX_SAFE_INTEGER);
