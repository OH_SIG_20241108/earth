function a() {
    var b;
    return {
        setters: [
            function c(b1) {
                b = b1;
            }
        ],
        execute: function () {
            let c = class c {
                constructor() {
                    this.m = 0;
                }
                d() {
                    const e = c.f;
                    return e;
                }
            };
            c.f = new b.g();
            return c;
        }
    };
}

let myModule = a();
myModule.setters[0]({ g: function () { console.log('Hello from g!'); } });
let MyClass = myModule.execute();
let myInstance = new MyClass();
myInstance.d();
