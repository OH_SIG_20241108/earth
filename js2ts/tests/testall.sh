#!/bin/sh
ulimit -s hard

# Check argument
if [ "$1" = "" ]; then 
    echo "Usage:  testall DIR"
    echo "  (where DIR is a directory tree containing JS files named using .js)"
    exit 99
fi

# Clean up previous results
./cleanall.sh "$1"

# Find JS files (only those named *.js)
for i in `find ${1} -type f | egrep -e '\.js$' | egrep -v '\.js\.js$'` ; do
    echo "=== $i ===" 
    (txl -s 400 -q -d STYLE -o $i.ts $i js2ts.txl $2 $3 $4 $5)
    (tsc -target es2021 --strict $i.ts) 
    (node $i.js) 
done

