export default function divide(num1, num2) {
    if (num2 === 0) {
      throw new Error('Division by zero');
    } else {
      return num1 / num2;
    }
  }
  