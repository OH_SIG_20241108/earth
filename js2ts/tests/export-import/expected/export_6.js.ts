export default function () : string {
    const name : string = "Jesse";
    const age : number = 40;
    return name + ' is ' + age + 'years old.';
}
