#!/bin/sh

# Check argument
dir=${1}
if [ "$dir" = "" ]; then 
    dir=.
fi

# Find test output files (only those named *.js.js, *.js.ts)
find ${dir} -type f | egrep -e '\.js\.ts$' | egrep -v 'expected/' | xargs /bin/rm
find ${dir} -type f | egrep -e '\.js\.js$' | egrep -v 'expected/' | xargs /bin/rm

