// variables such as i and total
var i = 10;
var total = 0;
// properties of objects
var obj = {}; // an empty object with no properties
obj.x = 10; // an assignment expression
// elements of arrays
let array = [] ;
array[0] = 20;
array[1] = 'hello';
//object property
var obj2 = {Proj:"js2ts", Year: 2023};
console.log(obj2.Proj, obj2.Year);