const a : number = 3;
const b : number = - 2;
console.log (a > 0 && b > 0);
console.log (a > 0 || b > 0);
const foo : null | string = null ?? 'default string';
console.log (foo);
const baz : number = 0 ?? 42;
console.log (baz);
