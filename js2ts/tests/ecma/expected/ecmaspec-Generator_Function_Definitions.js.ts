type JSDynamicObject = any;
const obj2 : JSDynamicObject = {
    g : function * () : Generator <number, any, any> {
        let index : number = 0;
        while (true) {
            yield index ++;
        }
    },
};
const it1 : any = obj2.g ();
console.log (it1.next ().value);
console.log (it1.next ().value);
const obj3 : JSDynamicObject = {
    * g () : any {
        let index : number = 0;
        while (true) {
            yield index ++;
        }
    },
};
const it : any = obj3.g ();
console.log (it.next ().value);
console.log (it.next ().value);
