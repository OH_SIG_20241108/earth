type JSDynamicObject = any;
const object1 : JSDynamicObject = {
    a : 'foo',
    b : 42,
    c : {}
};
console.log (object1.a);
const a : string = 'foo';
const b : number = 42;
const c : JSDynamicObject = {};
const object2 : JSDynamicObject = {
    a : a,
    b : b,
    c : c
};
console.log (object2.b);
const object3 : JSDynamicObject = {
    a,
    b,
    c
};
console.log (object3.a);
