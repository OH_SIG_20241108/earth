type JSDynamicObject = any;
function getUser (id : number) : JSDynamicObject {
    if (id <= 0) {
        return null;
    }
    return {
        id : id,
        username : 'admin',
        profile : {
            avatar : '/avatar.png',
            language : 'English'
        }
    };
}
let user : JSDynamicObject = getUser (2);
let profile : any = user ?.profile;
console.log (profile);
let file : JSDynamicObject = {
    read () : string {
        return 'file content';
    },
    write (content : any) : boolean {
        console.log (`Writing ${content} to file...`);
        return true;
    }
};
let compressedData : any = file.compress ?.();
console.log (compressedData);
