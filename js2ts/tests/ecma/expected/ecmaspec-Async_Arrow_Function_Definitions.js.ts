async function foo () : Promise <any> {
    console.log (await asyncFunction ());
}
function asyncFunction () : void {
    console.log ("async arrow function");
}
