function resolveAfter2Seconds (x : number) : Promise <any> {
    return new Promise <any> ((resolve : any) : any => {
            setTimeout (() : any => {
                    resolve (x);
                }, 2000);
        });
}
function myFunction (a : any, b : any) : any {
    return a * b;
}
const x : any = function (a : any, b : any) : any {
    return a * b;
}
;
let z : any = x (4, 3);
function * generate () : Generator <number, any, any> {
    console.log ('invoked 1st time');
    yield 1;
    console.log ('invoked 2nd time');
    yield 2;
}
async function add (x : any) : Promise <any> {
    const a : any = await resolveAfter2Seconds (20);
    const b : any = await resolveAfter2Seconds (30);
    return x + a + b;
}
async function * foo () : AsyncGenerator <any, any, any> {
    yield await Promise.resolve ('a');
    yield await Promise.resolve ('b');
    yield await Promise.resolve ('c');
}
let str : string = '';
async function generate1 () : Promise <any> {
    for await (const val of foo ()) {
        str = str + val;
    }
    console.log (str);
}
generate1 ();
