function resolveAfter2Seconds(x) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(x);
      }, 2000);
    });
}
  
function myFunction(a, b) {
    return a * b;
  }

const x = function (a, b) {return a * b};
let z = x(4, 3);

function* generate() {
    console.log('invoked 1st time');
    yield 1;
    console.log('invoked 2nd time');
    yield 2;
}

const add = async function (x) {
  const a = await resolveAfter2Seconds(20);
  const b = await resolveAfter2Seconds(30);
  return x + a + b;
};

async function* foo() {
  yield await Promise.resolve('a');
  yield await Promise.resolve('b');
  yield await Promise.resolve('c');
}

let str = '';

async function generate1() {
  for await (const val of foo()) {
    str = str + val;
  }
  console.log(str);
}

generate1();
// Expected output: "abc"