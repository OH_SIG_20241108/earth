class Test {
    name;
    age;
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    static value = 111;
    static foo() {
            return 222;
    }
}

let v1 = Test.value;
let v2 = Test.foo();


let t = new Test(123, "abc")
let v3 = t.name;
let v4 = t.age;
