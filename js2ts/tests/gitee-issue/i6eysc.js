var stringSet;
class C {
    get "doubleQuote"() { return 'get string'; }
    set "doubleQuote"(param) { stringSet = param; }
}
;
C.prototype["doubleQuote"] = 'set string';
console.log(stringSet);

var callCount = 0;
class D {
    method(fromLiteral = 23, fromExpr = 45, fromHole = 99) {
        callCount = callCount + 1;
    }
}
;
D.prototype.method(undefined, void 0);
console.log(callCount);
