let a = !true;
console.log(a);

let b = !"abc";
console.log(b);

let c = ![1, "a"];
console.log(c);

let d = !undefined;
console.log(d);

var objectx = {};
objectx.prop = true;
let v1 = objectx.prop;
let v = !v1;
console.log(v);
