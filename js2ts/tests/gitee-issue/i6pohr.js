// JS code
var a = 123;  // 1

{
    var a = "abc";  // 2
}

function foo() {
    var a = 123;  // 3
    function bar() {
        return a;  // 4
    }
    return a  // 5
}
