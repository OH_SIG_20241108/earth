let a1 = undefined + "";
console.log(a1);

let a2 = 0 + "";
console.log(a2);

let a3 = null + "";
console.log(a3);

let a4 = false + "";
console.log(a4);

let a5 = true + "";
console.log(a5);

let a6 = new Number() + "";
console.log(a6);

let a7 = new Number(0) + "";
console.log(a7);

let a8 = Number(Number.NaN) + "";
console.log(a8);

let a9 = new Number(null) + "";
console.log(a9);

let a10 = new Number(true) + "";
console.log(a10);

let a11 = new Number(false) + "";
console.log(a11);

let a12 = new Boolean(true) + "";
console.log(a12);

let a13 = new Boolean(false) + "";
console.log(a13);

let a14 = new Array(2, 4, 8, 16, 32) + "";
console.log(a14);
