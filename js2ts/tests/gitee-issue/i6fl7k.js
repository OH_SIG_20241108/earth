var f;
var callCount1 = 0;
f = function () {
    callCount1 = callCount1 + 1;
    return callCount1;
};
f();
console.log(callCount1);

var callCount2 = 0;
var g;
g = function ([[x, y, z] = [4, 5, 6]]) {
    callCount2++;
    return x + y + z;
};
console.log(g([]));
console.log(callCount2);

var probe;
(function () {
    var x = "inside";
    probe = function () { return x; };
}());
var x = "outside";
console.log(probe());
console.log(x);
