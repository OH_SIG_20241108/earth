let a = true || false;
console.log(a);

let b = "abc" || false;
console.log(b);

let c = [1, "a"] || 123;
console.log(c);

let d = true || undefined;
console.log(d);


var objectx = {};
var objecty = {};
objectx.prop = true;
objecty.prop = 1.1;
let v1 = objectx.prop;
let v2 = objecty.prop;
let v = v1 || v2;
console.log(v);
