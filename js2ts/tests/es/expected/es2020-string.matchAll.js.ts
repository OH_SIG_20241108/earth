const regex : RegExp = /t(e)(st(\d?))/g;
const string : string = 'test1test2';
const matchesIterator : IterableIterator <string[]|null> = string.matchAll (regex);
Array.from (matchesIterator, (result : any) : any => {
        return console.log (result);
    });
