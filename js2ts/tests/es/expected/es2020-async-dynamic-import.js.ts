(async function () : Promise <any> {
    const moduleSpecifier : string = './message.js';
    const messageModule : any = await import (moduleSpecifier);
    messageModule.default ();
    messageModule.sayGoodBye ();
} ());
