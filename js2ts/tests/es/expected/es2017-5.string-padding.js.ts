const cardNumber : string = '01234567891234';
const lastFourDigits : string = cardNumber.slice (- 4);
const maskedCardNumber : string = lastFourDigits.padStart (cardNumber.length, '*');
console.log (maskedCardNumber);
const label1 : string = "Name";
const label2 : string = "Phone Number";
const value1 : string = "John";
const value2 : string = "(222)-333-3456";
console.log ((label1 + ': ').padEnd (20, ' ') + value1);
console.log (label2 + ": " + value2);
