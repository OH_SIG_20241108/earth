type JSDynamicObject = any;
var a : number = 1;
var b : number = 2;
var c : number = 3;
var obj : JSDynamicObject = {
    a,
    b,
    c
};
console.log (obj);
var calculation : JSDynamicObject = {
    sum (a : any, b : any) : any {
        return a + b;
    },
    multiply (a : any, b : any) : any {
        return a * b;
    }
};
console.log (calculation.sum (5, 3));
console.log (calculation.multiply (5, 3));
const key : string = 'three';
const computedObj : JSDynamicObject = {
    one : 1,
    two : 2,
    [key] : 3
};
