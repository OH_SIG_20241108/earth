type JSDynamicObject = any;
let vehicle : JSDynamicObject = {
    car : {
        name : "",
        speed : 0
    }
};
console.log (vehicle.car.name || "Unknown");
console.log (vehicle.car.speed || 90);
console.log (vehicle.car.name ?? "Unknown");
console.log (vehicle.car.speed ?? 90);
