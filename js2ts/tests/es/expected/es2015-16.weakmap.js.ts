type JSDynamicObject = any;
var weakMap : WeakMap <any, any> = new WeakMap <any, any> ();
var obj1 : JSDynamicObject = {};
var obj2 : JSDynamicObject = {};
weakMap.set (obj1, 1);
weakMap.set (obj2, 2);
weakMap.set ({}, {
    "four" : 4
});
console.log (weakMap.get (obj2));
console.log (weakMap.has ({}));
console.log (weakMap.get (obj2));
weakMap.delete (obj1);
console.log (weakMap.get (obj1));
