class User {
    firstName : any;
    lastName : any;
    constructor (firstName : any, lastName : any) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    get fullName () : string {
        return `${this.firstName} ${this.lastName}`;
    }
}
;
let args : string [] = ['John', 'Emma'];
let john : any = Reflect.construct (User, args);
console.log (john instanceof User);
console.log (john.fullName);
const max : any = Reflect.apply (Math.max, Math, [100, 200, 300]);
console.log (max);
