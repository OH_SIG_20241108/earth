type JSDynamicObject = any;
function myfunc (p1 : number, p2 : number, ... p3 : number []) : void {
    console.log (p1, p2, p3);
}
myfunc (1, 2, 3, 4, 5, 6);
const myArray : number [] = [10, 5, 25, - 100, 200, - 200];
console.log (Math.max ( ... myArray));
function myfunc1 ({a, ... x} : any) : void {
    console.log (a, x);
}
myfunc1 ({
    a : 1,
    b : 2,
    c : 3,
    d : 4
});
const myObject : JSDynamicObject = {
    a : 1,
    b : 2,
    c : 3,
    d : 4
};
const myNewObject : JSDynamicObject = {
    ... myObject,
    e : 5
};
