var message = function (name) { return console.log("Hello, " + name + "!"); };
message("Sudheer");
var multiply = function (x, y) { return x * y; };
console.log(multiply(2, 3));
var even = function (number) {
    if (number % 2) {
        console.log("Even");
    }
    else {
        console.log("Odd");
    }
};
even(5);
var divide = function (x, y) {
    if (y != 0) {
        return x / y;
    }
};
console.log(divide(100, 5));
var greet = function () { return console.log('Hello World!'); };
greet();
