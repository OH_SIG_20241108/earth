// Default exports
// module "my-module.js"

// export default function add(...args) {
//     return args.reduce((num, tot) => tot + num);
// }

//Import Statement

import * as n from "./my-module.js";
// 1. Import an entire module's contents

import * as name from "./my-module.js";

//2.Import a single export from a module
//import { PI } from "./my-module.js";

//3.Import multiple exports from a module
 import { add , multiply } from "./my-module.js";

//4.Import default export from a module
import defaultExport from "./my-module.js";

//5.Import an export with an alias
import { print as alias1 } from "./my-module.js";

