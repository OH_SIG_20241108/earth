// Template literals
const firstName = 'John';
console.log(`Hello ${firstName}!
    Good morning!`);

// Tagged template literals
function tag(strings) {
  console.log(strings.raw[0]);
}

tag`string text line 1 \n string text line 2`;