#!/bin/sh
ulimit -s hard

# Check argument
if [ "$1" = "" ]; then 
    echo "Usage:  testall DIR"
    echo "  (where DIR is a directory tree containing JS files named using .js)"
    exit 99
fi

./cleanall.sh "$1"

# start a fifo to speed up
if [ "$2" = "" ]
then
    echo "Please add threadNum to boost. Now threadNum is 1."
    threadNum=1
elif [[ $2 =~ ^[0-9]+$ ]]
then
    threadNum=$2
else
    echo "Please put in a number!"
    exit 99
fi
fifofile="/tmp/js2ts.fifo"
mkfifo $fifofile
exec 8<> $fifofile
rm $fifofile

for i in `seq $threadNum`
do
    echo "" >&8
done

# JS2TS
echo "=== start JS2TS ==="

time1=$(date +%s)
for i in `find ${1} -type f | egrep -e '\.js$' | egrep -v '\.js\.js$'` ; do
    echo "=== $i ==="
    read -u 8
    {
        (txl -s 400 -q -d STYLE -o $i.ts -comment $i js2ts.txl)
        echo >&8
    }&
done
time2=$(date +%s)
js2ts_t=$(( time2 - time1 ))

echo "JS2TS costs $js2ts_t seconds"
