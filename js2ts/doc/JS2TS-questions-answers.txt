
Legal questions

1) After TXL is open source, if Huawei uses it, will there be disputes between Huawei and Queen's University?

	Once TXL is open source, Huawei will be bound only by the open source license.

	The planned license is the MIT open source license (https://opensource.org/licenses/MIT), which reads: 

		"Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
		 and associated documentation files (the "Software"), to deal in the Software without restriction, 
		 including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
		 and/or sell copies of the Software ..." 

	Queen's University and Jim Cordy will have no direct claim on it.

2) Where is TXL open-source hosted? Such as Github, Gitee, or another open source website?

	Initially open source TXL (OpenTXL) will be hosted on the TXL website.
	Once converted to C, the plan is to host it on Github.
	However, I recommend that Huawei make and maintain their own copy.

3) Who will be the owner of the open source, Dr. Cordy or Queen's University?

	The original copyright is held by both, but the license does not restrict any use 
	or give the copyright holders any rights of ownership other than recognition.

4) Who will continue to maintain it after the source is opened? Does it need Huawei people to maintain it?

	Open source TXL will be maintained by the open source community like other open source projects.
	If Huawei makes their own copy, it will be maintained by Huawei people.
	If ongoing support is required for either, Prof. Cordy is available on a consulting basis to help. 


Technical questions

1) After rewriting it in C, what is the performance as estimated? For example, how much time will the tool spend for 10K-line js code? Because we may consider integrating it into Ark compiler.

	The performance of TXL in C will be identical to performance of present production TXL in Turing+.
	The performance of the JS2TS tool depends on how its TXL rules are written and optimized.
	JS2TS inference rules prototyped in TXL could be encoded into the Ark compiler as manipulations of the Ark ASTs, independently of TXL.

2) What about optional-properties? For example, some object properties are added after running to a specific if block, which is a bit different from the direct declaration of statically typed language.

	At present the JS2TS prototype assumes that all object properties (i.e., "this.x" references) 
	mentioned anywhere in the code are properties of the class, and creates a property declaration for them.

3) If most of them are object-literal types, what will the inferring achieve? The object-literal type means similar to let a = { x: 1, y: "hello"}, which is actually similar to class or even simpler

	This is an open question and the prototype does not yet address it. We can either make a rule to infer a class type 
	and give it a name (e.g., class aclass { x:1, y: "hello"}; let a = new aclass(); ) or simply handle object literals directly.

	The present prototype JS2TS is only an experiment that handles a few of the inference rules that will be needed.
	A complete implementation will take time to develop and require many many more inference rules.
	Any inference rule we can think of can be encoded in TXL and easily added.

