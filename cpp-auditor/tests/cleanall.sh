#!/bin/sh

# Check argument
dir=${1}
if [ "$dir" = "" ]; then 
    dir=.
fi

# Find test output files (only those named *.cpp.audit.cpp)
find ${dir} -type f | egrep -e '\.cpp\.audit\.cpp$' | egrep -v 'expected/' | xargs /bin/rm
