#include "napi/native_api.h"
#include "NodeMyCppInterfaceCpp.hpp"
#include "NodeMyClientInterfaceCpp.hpp"
#include "NodeMyTsInterfaceCpp.hpp"
#include "NodeMyTsInterface2Cpp.hpp"

EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports)
{
    NodeMyCppInterface::Init(env, exports);
    NodeMyClientInterface::Init(env, exports);
    NodeMyTsInterface::Init(env, exports);
    NodeMyTsInterface2::Init(env, exports);

    return exports;
}
EXTERN_C_END

static napi_module callbackModule = 
{
    .nm_version = 1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = Init,
    .nm_modname = "entry",
    .nm_priv = ((void*)0),
    .reserved = {0},
};

extern "C" __attribute__((constructor)) void CallbackTestRegister()
{
    napi_module_register(&callbackModule);
}
