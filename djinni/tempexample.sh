CPP_OUTPUT_FOLDER=target/djinni-example-temp/cpp
ARKTS_OUTPUT_FOLDER=target/djinni-example-temp/arkts
MY_PROJECT=target/djinni-example-temp/proj-temp.djinni


src/run --arkts-out $ARKTS_OUTPUT_FOLDER --arkts-type-prefix ARKTS --arkts-module TempExample --arkts-include-cpp $CPP_OUTPUT_FOLDER --cpp-optional-header absl/types/optional.h --cpp-optional-template absl:optional --cpp-out $CPP_OUTPUT_FOLDER --idl $MY_PROJECT