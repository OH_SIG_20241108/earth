// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from primitive_list.djinni

#ifndef DJINNI_GENERATED_PRIMITIVE_LIST_HPP
#define DJINNI_GENERATED_PRIMITIVE_LIST_HPP

#include <cstdint>
#include <iostream>
#include <utility>
#include <vector>

namespace testsuite {

struct PrimitiveList final {
    std::vector<int64_t> list;

    PrimitiveList(std::vector<int64_t> list_)
    : list(std::move(list_))
    {}

    PrimitiveList(const PrimitiveList& cpy) {
       this->list = cpy.list;
    }

    PrimitiveList() = default;


    PrimitiveList& operator=(const PrimitiveList& cpy) {
       this->list = cpy.list;
       return *this;
    }

    template <class Archive>
    void load(Archive& archive) {
        archive(list);
    }

    template <class Archive>
    void save(Archive& archive) const {
        archive(list);
    }
};

}  // namespace testsuite
#endif //DJINNI_GENERATED_PRIMITIVE_LIST_HPP
