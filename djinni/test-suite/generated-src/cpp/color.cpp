// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from enum.djinni

#include "color.hpp"  // my header
#include "enum_from_string.hpp"

namespace testsuite {

std::string to_string(const color& color) {
    switch (color) {
        case color::RED: return "RED";
        case color::ORANGE: return "ORANGE";
        case color::YELLOW: return "YELLOW";
        case color::GREEN: return "GREEN";
        case color::BLUE: return "BLUE";
        case color::INDIGO: return "INDIGO";
        case color::VIOLET: return "VIOLET";
        default: return "UNKNOWN";
    };
};
template <>
color from_string(const std::string& color) {
    if (color == "RED") return color::RED;
    else if (color == "ORANGE") return color::ORANGE;
    else if (color == "YELLOW") return color::YELLOW;
    else if (color == "GREEN") return color::GREEN;
    else if (color == "BLUE") return color::BLUE;
    else if (color == "INDIGO") return color::INDIGO;
    else return color::VIOLET;
};

std::ostream &operator<<(std::ostream &os, const color &o)
{
    switch (o) {
        case color::RED:  return os << "RED";
        case color::ORANGE:  return os << "ORANGE";
        case color::YELLOW:  return os << "YELLOW";
        case color::GREEN:  return os << "GREEN";
        case color::BLUE:  return os << "BLUE";
        case color::INDIGO:  return os << "INDIGO";
        case color::VIOLET:  return os << "VIOLET";
    }
}

}  // namespace testsuite
