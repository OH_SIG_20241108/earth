// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from derivings.djinni

#ifndef DJINNI_GENERATED_RECORD_WITH_DERIVINGS_HPP
#define DJINNI_GENERATED_RECORD_WITH_DERIVINGS_HPP

#include <chrono>
#include <cstdint>
#include <iostream>
#include <string>
#include <utility>

namespace testsuite {

struct RecordWithDerivings final {
    int8_t eight;
    int16_t sixteen;
    int32_t thirtytwo;
    int64_t sixtyfour;
    float fthirtytwo;
    double fsixtyfour;
    std::chrono::system_clock::time_point d;
    std::string s;

    friend bool operator==(const RecordWithDerivings& lhs, const RecordWithDerivings& rhs);
    friend bool operator!=(const RecordWithDerivings& lhs, const RecordWithDerivings& rhs);

    friend bool operator<(const RecordWithDerivings& lhs, const RecordWithDerivings& rhs);
    friend bool operator>(const RecordWithDerivings& lhs, const RecordWithDerivings& rhs);

    friend bool operator<=(const RecordWithDerivings& lhs, const RecordWithDerivings& rhs);
    friend bool operator>=(const RecordWithDerivings& lhs, const RecordWithDerivings& rhs);

    RecordWithDerivings(int8_t eight_,
                        int16_t sixteen_,
                        int32_t thirtytwo_,
                        int64_t sixtyfour_,
                        float fthirtytwo_,
                        double fsixtyfour_,
                        std::chrono::system_clock::time_point d_,
                        std::string s_)
    : eight(std::move(eight_))
    , sixteen(std::move(sixteen_))
    , thirtytwo(std::move(thirtytwo_))
    , sixtyfour(std::move(sixtyfour_))
    , fthirtytwo(std::move(fthirtytwo_))
    , fsixtyfour(std::move(fsixtyfour_))
    , d(std::move(d_))
    , s(std::move(s_))
    {}

    RecordWithDerivings(const RecordWithDerivings& cpy) {
       this->eight = cpy.eight;
       this->sixteen = cpy.sixteen;
       this->thirtytwo = cpy.thirtytwo;
       this->sixtyfour = cpy.sixtyfour;
       this->fthirtytwo = cpy.fthirtytwo;
       this->fsixtyfour = cpy.fsixtyfour;
       this->d = cpy.d;
       this->s = cpy.s;
    }

    RecordWithDerivings() = default;


    RecordWithDerivings& operator=(const RecordWithDerivings& cpy) {
       this->eight = cpy.eight;
       this->sixteen = cpy.sixteen;
       this->thirtytwo = cpy.thirtytwo;
       this->sixtyfour = cpy.sixtyfour;
       this->fthirtytwo = cpy.fthirtytwo;
       this->fsixtyfour = cpy.fsixtyfour;
       this->d = cpy.d;
       this->s = cpy.s;
       return *this;
    }

    template <class Archive>
    void load(Archive& archive) {
        archive(eight, sixteen, thirtytwo, sixtyfour, fthirtytwo, fsixtyfour, d, s);
    }

    template <class Archive>
    void save(Archive& archive) const {
        archive(eight, sixteen, thirtytwo, sixtyfour, fthirtytwo, fsixtyfour, d, s);
    }
};

}  // namespace testsuite
#endif //DJINNI_GENERATED_RECORD_WITH_DERIVINGS_HPP
